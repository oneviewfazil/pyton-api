﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessEntities.Entity;

namespace DAL.Persistence.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    public class MemoryDataGenerator
    {
        private readonly Random _rnd = new Random();
        List<City> _cities = new List<City>();
        List<Flight> _flight = new List<Flight>();
        public MemoryDataSource Generate(int dataCount)
        {
            var dataSource = new MemoryDataSource();

            //BASE DATA
            var personNames = GeneratePersonNames();

            ////CITIES
            _flight.AddRange(
                new[]
                {
                    new Flight {FlightName = "Air India"},
                    new Flight {FlightName = "Jet Airways"},
                    new Flight {FlightName = "IndiGo"},
                    new Flight {FlightName = "SpiceJet"},
                    new Flight {FlightName = "GoAir"},
                    new Flight {FlightName = "Vistara Airlines"},
                    new Flight {FlightName = "JetLite"},
                    new Flight {FlightName = "Air India Express"},
                    new Flight {FlightName = "Qatar Airways"},
                    new Flight {FlightName = "Emirates Airline"},
                    new Flight {FlightName = "Oman Airline"},
                    new Flight {FlightName = "Saudi Airline"},
                    new Flight {FlightName = "British Airline"},
                    new Flight {FlightName = "Turkey Airline"},
                    new Flight {FlightName = "Pakistan Airline"}
                });

            ////CITIES
            _cities.AddRange(
                new[]
                {
                    new City {CityId = 1, CityName = "Adana"},
                    new City {CityId = 2, CityName = "Ankara"},
                    new City {CityId = 3, CityName = "Athens"},
                    new City {CityId = 4, CityName = "Beijing"},
                    new City {CityId = 5, CityName = "Berlin"},
                    new City {CityId = 6, CityName = "Bursa"},
                    new City {CityId = 7, CityName = "İstanbul"},
                    new City {CityId = 8, CityName = "London"},
                    new City {CityId = 9, CityName = "Madrid"},
                    new City {CityId = 10, CityName = "Mekke"},
                    new City {CityId = 11, CityName = "New York"},
                    new City {CityId = 12, CityName = "Paris"},
                    new City {CityId = 13, CityName = "Samsun"},
                    new City {CityId = 14, CityName = "Trabzon"},
                    new City {CityId = 15, CityName = "Volos"}
                });

            //int loop = GetLoopCount();
            //for (int i = 0; i < loop; i++)
            //{
                var nameIndex = _rnd.Next(personNames.Length);
                var surnameIndex = _rnd.Next(personNames.Length);
                var partner = new PartnerEntity
                {
                    PartnerId = _rnd.Next(personNames.Length),
                    Name = personNames[nameIndex].Name + " " + personNames[surnameIndex].Surname,
                    Contact = GenerateRandomPhoneNumber(),
                    Email =
                        personNames[nameIndex].Name.ToLower() + "." +
                        personNames[surnameIndex].Surname.ToLower() + "@gmail.org",
                    Address = "Doha Qatar",
                };
                List<PartnerFlightEntity> list = new List<PartnerFlightEntity>();
               
                for (int j = 0; j < dataCount; j++)
                {
                    list.Add(new PartnerFlightEntity() {
                        FlightDate = new DateTime(_rnd.Next(2018, 2019), _rnd.Next(1, 13), _rnd.Next(1, 29)),
                        FlightFrom = _cities[_rnd.Next(1, 15)].CityName,
                        FlightTo = _cities[_rnd.Next(1, 15)].CityName,
                        FlightName = _flight[_rnd.Next(1, 15)].FlightName,
                        FlightNo = RandomString(_rnd.Next(3, 6)) + " " + _rnd.Next(3, 6).ToString(),
                         SeatsAvailable = _rnd.Next(10, 50)
                    });
                }
                partner.PartnerFlightsEntity = list;
                dataSource.Partners.Add(partner);
            //}
            return dataSource;
        }

        private int GetLoopCount()
        {
            return _rnd.Next(2, 5);
        }

        private string GenerateRandomPhoneNumber()
        {
            var sb = new StringBuilder();
            for (int i = 0; i < 10; i++)
            {
                sb.Append(_rnd.Next(0, 10).ToString());
            }

            return sb.ToString();
        }
        private string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[_rnd.Next(s.Length)]).ToArray());
        }

        private static PersonNameSurname[] GeneratePersonNames()
        {
            return new[]
                             {
                                 new PersonNameSurname {Name = "Halil", Surname = "Kalkan", Gender = "M"},
                                 new PersonNameSurname {Name = "Karen", Surname = "Asimov", Gender = "F"},
                                 new PersonNameSurname {Name = "Neo", Surname = "Gates", Gender = "M"},
                                 new PersonNameSurname {Name = "Trinity", Surname = "Lafore", Gender = "F"},
                                 new PersonNameSurname {Name = "Morpheus", Surname = "Maalouf", Gender = "M"},
                                 new PersonNameSurname {Name = "Suzanne", Surname = "Hayyam", Gender = "F"},
                                 new PersonNameSurname {Name = "Georghe", Surname = "Richards", Gender = "M"},
                                 new PersonNameSurname {Name = "Steeve", Surname = "Orwell", Gender = "M"},
                                 new PersonNameSurname {Name = "Agatha", Surname = "Jobs", Gender = "F"},
                                 new PersonNameSurname {Name = "Stephan", Surname = "Christie", Gender = "M"},
                                 new PersonNameSurname {Name = "Andrew", Surname = "Hawking", Gender = "M"},
                                 new PersonNameSurname {Name = "Nicole", Surname = "Brown", Gender = "F"},
                                 new PersonNameSurname {Name = "Thomas", Surname = "Garder", Gender = "M"},
                                 new PersonNameSurname {Name = "Oktay", Surname = "More", Gender = "M"},
                                 new PersonNameSurname {Name = "Paulho", Surname = "Anar", Gender = "M"},
                                 new PersonNameSurname {Name = "Carl", Surname = "Sagan", Gender = "M"},
                                 new PersonNameSurname {Name = "Daniel", Surname = "Radcliffe", Gender = "F"},
                                 new PersonNameSurname {Name = "Rupert", Surname = "Grint", Gender = "M"},
                                 new PersonNameSurname {Name = "David", Surname = "Yates", Gender = "M"},
                                 new PersonNameSurname {Name = "Hercules", Surname = "Poirot", Gender = "M"},
                                 new PersonNameSurname {Name = "Christopher", Surname = "Paolini", Gender = "M"},
                                 new PersonNameSurname {Name = "Walter", Surname = "Isaacson", Gender = "M"},
                                 new PersonNameSurname {Name = "Arda", Surname = "Turan", Gender = "M"},
                                 new PersonNameSurname {Name = "Jeniffer", Surname = "Anderson", Gender = "F"},
                                 new PersonNameSurname {Name = "Stephenie", Surname = "Mayer", Gender = "F"},
                                 new PersonNameSurname {Name = "Dan", Surname = "Brown", Gender = "M"},
                                 new PersonNameSurname {Name = "Clara", Surname = "Clayton", Gender = "F"},
                                 new PersonNameSurname {Name = "Emmett", Surname = "Brown", Gender = "M"},
                                 new PersonNameSurname {Name = "Marty", Surname = "Mcfly", Gender = "M"},
                                 new PersonNameSurname {Name = "Jane", Surname = "Fuller", Gender = "F"},
                                 new PersonNameSurname {Name = "Douglas", Surname = "Hall", Gender = "M"},
                                 new PersonNameSurname {Name = "Tom", Surname = "Jones", Gender = "M"},
                                 new PersonNameSurname {Name = "Lora", Surname = "Adams", Gender = "F"},
                                 new PersonNameSurname {Name = "Andy", Surname = "Garcia", Gender = "M"},
                                 new PersonNameSurname {Name = "Amin", Surname = "Collins", Gender = "M"},
                                 new PersonNameSurname {Name = "Elmander", Surname = "Sokrates", Gender = "M"},
                                 new PersonNameSurname {Name = "Austin", Surname = "Cleeve", Gender = "F"},
                                 new PersonNameSurname {Name = "Audrey", Surname = "Cole", Gender = "F"},
                                 new PersonNameSurname {Name = "Bella", Surname = "Clark", Gender = "F"},
                                 new PersonNameSurname {Name = "Burley", Surname = "Pugy", Gender = "M"},
                                 new PersonNameSurname {Name = "Charles", Surname = "Quiney", Gender = "M"}
                             };
        }
        private class PersonNameSurname
        {
            public string Name { get; set; }
            public string Surname { get; set; }
            public string Gender { get; set; }
        }
    }
    public class City
    {
        public int CityId { get; set; }
        public string CityName { get; set; }
    }
    public class Flight
    {
        public string FlightName { get; set; }
    }
}
