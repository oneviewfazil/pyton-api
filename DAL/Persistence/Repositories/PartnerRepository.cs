﻿namespace DAL.Persistence.Repositories
{
    using System.Collections.Generic;
    using BusinessEntities.Entity;
    using System;
    using System.Threading.Tasks;
    using Core.Repositories;
    using System.Linq;

    public class PartnerRepository : IPartnerRepository
    {
        private readonly MemoryDataSource _dataSource;

        public PartnerRepository(MemoryDataSource dataSource)
        {
            _dataSource = dataSource;
        }

        public Task<List<PartnerEntity>> GetAsyncPartnerData()
        {
            throw new NotImplementedException();
        }

        public List<PartnerEntity> GetPartnerData()
        {
            return _dataSource.Partners.ToList();

        }

    }
}
