﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL.Core.Repositories;

namespace DAL.Persistence.Repositories
{
    public class MemoryRepositoryContainer : IRepositoryContainer
    {
        private readonly MemoryDataSource _dataSource;

        public MemoryRepositoryContainer(MemoryDataSource dataSource)
        {
            _dataSource = dataSource;
        }

        public IPartnerRepository PartnerRepository
        {
            get { return new PartnerRepository(_dataSource); }
        }

        
    }
}
