﻿namespace DAL
{
    using System.ComponentModel.Composition;
    using Resolver;
    using DAL.Core.Repositories;
    using DAL.Persistence.Repositories;


    [Export(typeof(IComponent))]
    public class DependencyResolver : IComponent
    {
        public void SetUp(IRegisterComponent registerComponent)
        {
            //registerComponent.RegisterType<IPartnerRepository, PartnerRepository>();

        }
    }
}
