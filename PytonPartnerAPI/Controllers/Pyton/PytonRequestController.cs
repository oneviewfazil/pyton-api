﻿using System.Net;
using System.Net.Http;
using System.Web.Http;
using PytonPartnerAPI.ErrorHelper;
using BusinessEntities;
using PytonPartner.BusinessServices.Interface;
using System.Threading.Tasks;
using System.Web.Http.Description;

namespace PytonPartnerAPI.Controllers.Pyton
{
    [RoutePrefix("python")]
    public class PytonRequestController : ApiController
    {
        private readonly IPytonRequest pytonRequest;

        public PytonRequestController(IPytonRequest _pytonRequest)
        {
            this.pytonRequest = _pytonRequest;
        }


        [HttpPost]
        [Route("search/flight")]
        [ResponseType(typeof(Rootobject))]
        public HttpResponseMessage GetFlights(Rootobject data)
        {
            var response = pytonRequest.FlightSearchSync(data);

            if (data != null)
                return Request.CreateResponse(HttpStatusCode.OK, response);

            throw new ApiDataException(1000, "flight not found", HttpStatusCode.NotFound);
        }
        [HttpPost]
        [Route("validate/flight")]
        [ResponseType(typeof(Rootobject))]
        public HttpResponseMessage ValidateFlight(FareRevalidateRQ data)
        {
            var response = pytonRequest.Farevalidate(data);

            if (data != null)
                return Request.CreateResponse(HttpStatusCode.OK, response);

            throw new ApiDataException(1000, "flight not found", HttpStatusCode.NotFound);
        }

        [HttpPost]
        [Route("preparebook/flight")]
        [ResponseType(typeof(Rootobject))]
        public HttpResponseMessage prepareBookFlight(FLbookRQ data)
        {
            var response = pytonRequest.Preparebookflight(data);

            if (data != null)
                return Request.CreateResponse(HttpStatusCode.OK, response);

            throw new ApiDataException(1000, "flight not found", HttpStatusCode.NotFound);
        }

        [HttpPost]
        [Route("book/flight")]
        [ResponseType(typeof(Rootobject))]
        public HttpResponseMessage BookFlight(FLbookRQ data)
        {
            var response = pytonRequest.Bookflight(data);

            if (data != null)
                return Request.CreateResponse(HttpStatusCode.OK, response);

            throw new ApiDataException(1000, "flight not found", HttpStatusCode.NotFound);
        }

        [HttpPost]
        [Route("bookdetails/flight")]
        [ResponseType(typeof(Rootobject))]
        public HttpResponseMessage Bookdetails(TripDetailscomRQ data)
        {
            var response = pytonRequest.Bookflightdetails(data);

            if (data != null)
                return Request.CreateResponse(HttpStatusCode.OK, response);

            throw new ApiDataException(1000, "Data not found", HttpStatusCode.NotFound);
        }

    }
}
