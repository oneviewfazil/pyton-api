﻿using BusinessEntities;
using PytonPartner.PytonPartnerServiceReference;
using System.Threading.Tasks;

namespace PytonPartner.BusinessServices.Interface
{
    public interface IPytonRequest
    {
        // string pytonreq();

        Task<FLcommonRS> FlightSearch(Rootobject message);
        FLcommonRS FlightSearchSync(Rootobject message);

        FareRevalidateRS Farevalidate(FareRevalidateRQ message);


        FLbookRS Preparebookflight(FLbookRQ  message);

        FLbookRS Bookflight(FLbookRQ message);
        TripDetailsCom_RS Bookflightdetails(TripDetailscomRQ message);



    }
}