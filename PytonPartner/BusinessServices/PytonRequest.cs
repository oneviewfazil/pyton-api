﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PytonPartner.PytonPartnerServiceReference;
using PytonPartner.BusinessServices.Interface;
using Newtonsoft.Json.Linq;
using BusinessEntities;
using System.Xml.Serialization;
using System.IO;
using System.ServiceModel;

using System.ServiceModel.Channels;
using System.Net;
using System.Web.Services.Protocols;
using System.Diagnostics;

namespace PytonPartner.BusinessServices
{
    public class PytonRequest : IPytonRequest
    {
        public FLcommonRS FlightSearchSync(Rootobject message)
        {

            WebServiceSoapClient client = new WebServiceSoapClient("WebServiceSoap");
            int count3 = message.OriginDestinationInformation.Length;
            string Year = ""; string Month = ""; string Day = "";
            try
            {
                Year = (message.OriginDestinationInformation[0].DepartureDate).Split('-')[2];
                Month = (message.OriginDestinationInformation[0].DepartureDate).Split('-')[1];
                Day = (message.OriginDestinationInformation[0].DepartureDate).Split('-')[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }

            Passengertypequantity ps = new Passengertypequantity();


            SearchFlightsRequest req = new SearchFlightsRequest();

            req.Departure = message.OriginDestinationInformation[0].OriginLocation;
            req.Destination = message.OriginDestinationInformation[0].DestinationLocation;
            req.DepartureDate = Year + "-" + Month + "-" + Day;
            if (message.Triptype == "R")
            {
                string Year1 = ""; string Month1 = ""; string Day1 = "";
                try
                {
                    Year1 = (message.OriginDestinationInformation[1].DepartureDate).Split('-')[2];
                    Month1 = (message.OriginDestinationInformation[1].DepartureDate).Split('-')[1];
                    Day1 = (message.OriginDestinationInformation[1].DepartureDate).Split('-')[0];
                    req.ReturnDate = Year1 + "-" + Month1 + "-" + Day1;
                }
                catch (Exception ex)
                {
                    throw ex;
                }


            }
            req.NumADT = message.PassengerTypeQuantity.ADT;
            req.NumINF = message.PassengerTypeQuantity.INF;
            req.NumCHD = message.PassengerTypeQuantity.CHD;
            string adtcount = message.PassengerTypeQuantity.ADT.ToString();
            string chdcount = message.PassengerTypeQuantity.CHD.ToString();
            string infcount = message.PassengerTypeQuantity.INF.ToString();
            switch (message.Currency)
            {
                case "AED":
                    req.CurrencyCode = CurrencyCode.AED;
                    break;
                case "USD":
                    req.CurrencyCode = CurrencyCode.USD;
                    break;
                case "EUR":
                    req.CurrencyCode = CurrencyCode.USD;
                    break;

            }

            switch (message.IsRefundable)
            {
                case "true":
                    req.WaitForResult = true;
                    break;
                case "false":
                    req.WaitForResult = false;
                    break;
                    default:
                    req.WaitForResult = true;
                    break;



            }



            switch (message.Maxstopquantity)
            {
                case "true":
                    req.NearbyDepartures = true;
                    break;
                case "false":
                    req.NearbyDepartures = false;
                    break;
                default:
                    req.NearbyDepartures = false;
                    break;

            }

            switch (message.PreferenceLevel)
            {
                case "true":
                    req.NearbyDestinations = true;
                    break;
                case "false":
                    req.NearbyDestinations = false;
                    break;
                default:
                    req.NearbyDestinations = false;
                    break;

            }

            switch (message.PreferredAirline)
            {
                case "True":
                    req.RROnly = true;
                    break;
                case "false":
                    req.RROnly = false;
                    break;
                default:
                    req.RROnly = false;
                    break;

            }
            req.MetaSearch = false;
            Provider[] prov = new Provider[] {
                 Provider.ElsyArres,
             };
            req.Providers = prov;
            SearchFlights sfl = new SearchFlights();
            //sfl.Username = "Albadie";
            //sfl.Password = "01E666EA35";
            sfl.Username = message.SupplierAgencyDetails[0].UserName;
            sfl.Password = message.SupplierAgencyDetails[0].Password;
            sfl.LanguageCode = LanguageCode.EN;
            sfl.AppVersion = "";
            sfl.Request = req;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12 | SecurityProtocolType.Ssl3;
            try
            {
                client.SearchFlights(ref sfl);
            }

            catch (SoapException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            var response = sfl.Response;
            FLcommonRS fLcommonRS = new FLcommonRS();
            string AgencyCode = message.SupplierAgencyDetails[0].AgencyCode.ToString();
            string rQ = Newtonsoft.Json.JsonConvert.SerializeObject(sfl.Request);
            string rS = Newtonsoft.Json.JsonConvert.SerializeObject(response);
            BusinessEntities.Common.CustomExtention.SaveLog("FLIGHTDETAILS", AgencyCode, "PYT001", rQ, rS);
            fLcommonRS = PytonTOCommonRs(response, adtcount, chdcount, infcount);
            return fLcommonRS;

        }

        public FareRevalidateRS Farevalidate(FareRevalidateRQ message)
        {
            WebServiceSoapClient client = new WebServiceSoapClient();
            GetFlightDetails req = new GetFlightDetails();
            GetFlightDetailsRequest req1 = new GetFlightDetailsRequest();
            Flightidentification rq = new Flightidentification();
            //req.Username = "Albadie";
            //req.Password = "01E666EA35";
            req.Username = message.CommonRequestFarePricer.SupplierAgencyDetails[0].UserName;
            req.Password= message.CommonRequestFarePricer.SupplierAgencyDetails[0].Password;
            req.LanguageCode = LanguageCode.EN;
            req1.FlightId = message.CommonRequestFarePricer.Body.AirRevalidate.FareSourceCode;
            req.Request = req1;
            string faresource = message.CommonRequestFarePricer.Body.AirRevalidate.FareSourceCode;
            string carddt = message.CommonRequestFarePricer.Body.AirRevalidate.paymentCardType;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12 | SecurityProtocolType.Ssl3;
            client.GetFlightDetails(ref req);
            var response = req.Response;
            string AgencyCode = message.CommonRequestFarePricer.SupplierAgencyDetails[0].AgencyCode.ToString();
            string rQ = Newtonsoft.Json.JsonConvert.SerializeObject(req.Request);
            string rS = Newtonsoft.Json.JsonConvert.SerializeObject(response);
            BusinessEntities.Common.CustomExtention.SaveLog("FAREVALIDATE", AgencyCode, "PYT001", rQ, rS);
            FareRevalidateRS fLcommonRS = new FareRevalidateRS();
            fLcommonRS = PytonValidateTOCommonRs(response, faresource,carddt);
            return fLcommonRS;
        }

        public FLbookRS Preparebookflight(FLbookRQ message)
        {

          WebServiceSoapClient client = new WebServiceSoapClient();
            FLbookRQ req = new FLbookRQ();
            PrepareBookFlightsRequest book = new PrepareBookFlightsRequest();
            PytonPartnerServiceReference.CustomerInfo con = new PytonPartnerServiceReference.CustomerInfo();
            PytonPartnerServiceReference.PaymentInfo pay = new PytonPartnerServiceReference.PaymentInfo();
            PrepareBookFlights req1 = new PrepareBookFlights();
            PytonPartnerServiceReference.BillingAddress bill = new PytonPartnerServiceReference.BillingAddress();
            Fdetails rd = new Fdetails();

            try
            {
                book.FlightId = message.BookFlight.Faresourcecode;
                float confirmpricecent = float.Parse(message.BookFlight.Fdetails.ConfirmedPrice) * 100;
                book.ConfirmedPrice = int.Parse(confirmpricecent.ToString());
                switch (message.BookFlight.Fdetails.ConfirmedCurrency)
                {
                    case "AED":
                        book.ConfirmedCurrency = CurrencyCode.AED;
                        break;
                    case "USD":
                        book.ConfirmedCurrency = CurrencyCode.USD;
                        break;
                    case "EUR":
                        book.ConfirmedCurrency = CurrencyCode.EUR;
                        break;

                }
                book.ConfirmPossibleDuplicate = false;
                con.FirstName = message.BookFlight.CustomerInfo.FirstName;
                con.LastName = message.BookFlight.CustomerInfo.LastName;
                con.CompanyName = message.BookFlight.CustomerInfo.CompanyName;
                con.Street = message.BookFlight.CustomerInfo.Street;
                con.HouseNo = message.BookFlight.CustomerInfo.HouseNo;
                con.Zip = message.BookFlight.CustomerInfo.Zip;
                con.City = message.BookFlight.CustomerInfo.City;
                con.CountryCode = CountryCode.AF;
                con.PhoneCountry = message.BookFlight.CustomerInfo.PhoneCountry;
                con.PhoneArea = message.BookFlight.CustomerInfo.PhoneArea;
                con.PhoneNumber = message.BookFlight.CustomerInfo.PhoneNumber;
                con.Email = message.BookFlight.CustomerInfo.Email;
                book.CustomerInfo = con;
                switch (message.BookFlight.PaymentInfo.PaymentCode)
                {
                    case "AIRPLUS":
                        pay.PaymentCode = PaymentCode.AIRPLUS;
                        break;
                    case "AMEX":
                        pay.PaymentCode = PaymentCode.AMEX;
                        break;
                    case "CARTEBLEU":
                        pay.PaymentCode = PaymentCode.CARTEBLEU;
                        break;
                    case "CBC":
                        pay.PaymentCode = PaymentCode.CBC;
                        break;
                    case "DINERS":
                        pay.PaymentCode = PaymentCode.DINERS;
                        break;
                    case "DIRECTEBANKING":
                        pay.PaymentCode = PaymentCode.DIRECTEBANKING;
                        break;
                    case "ELECTRON":
                        pay.PaymentCode = PaymentCode.ELECTRON;
                        break;
                    case "ELSYARRES":
                        pay.PaymentCode = PaymentCode.ELSYARRES;
                        break;
                    case "ELV":
                        pay.PaymentCode = PaymentCode.ELV;
                        break;
                    case "IDEAL":
                        pay.PaymentCode = PaymentCode.IDEAL;
                        break;
                    case "JCB":
                        pay.PaymentCode = PaymentCode.JCB;
                        break;
                    case "KBC":
                        pay.PaymentCode = PaymentCode.KBC;
                        break;
                    case "MAESTRO":
                        pay.PaymentCode = PaymentCode.MAESTRO;
                        break;
                    case "MASTERCARD":
                        pay.PaymentCode = PaymentCode.MASTERCARD;
                        break;
                    case "MASTERDEBIT":
                        pay.PaymentCode = PaymentCode.MASTERDEBIT;
                        break;
                    case "MASTERPREPAID":
                        pay.PaymentCode = PaymentCode.MASTERPREPAID;
                        break;
                    case "UATP":
                        pay.PaymentCode = PaymentCode.UATP;
                        break;
                    case "VISA":
                        pay.PaymentCode = PaymentCode.VISA;
                        break;
                    case "VISADEBIT":
                        pay.PaymentCode = PaymentCode.VISADEBIT;
                        break;
                    default:
                        pay.PaymentCode = PaymentCode.VISA;
                        break;

                }
               // pay.PaymentCode = PaymentCode.VISA;
                pay.Holder = message.BookFlight.PaymentInfo.Holder;
                pay.Number = message.BookFlight.PaymentInfo.Number;
                pay.CVC = message.BookFlight.PaymentInfo.CVC;
                pay.Expiry = message.BookFlight.PaymentInfo.Expiry;
                bill.CountryCode = CountryCode.AF;
                pay.BillingAddress = bill;
                book.PaymentInfo = pay;
                int plenghth = message.BookFlight.TravelerInfo.Count;
                // string PaxType = request.TravelerInfo[FLTravellers].PassengerTypeQuantity.ToUpper();
                Passenger[] pas = new Passenger[plenghth];
                int i = 0;
                message.BookFlight.TravelerInfo.ForEach(j =>
                {
                    Passenger pa = new Passenger();
                    PassportDetails pass = new PassportDetails();
                    string PaxType = message.BookFlight.TravelerInfo[i].PassengerTypeQuantity.ToUpper();
                    switch (PaxType)
                    {
                        case "ADT":
                            pa.Type = PytonPartnerServiceReference.PaxType.ADULT;
                            break;
                        case "CHD":
                            pa.Type = PytonPartnerServiceReference.PaxType.CHILD;
                            break;
                        case "INF":
                            pa.Type = PytonPartnerServiceReference.PaxType.INFANT;
                            break;


                    }

                    string gender = message.BookFlight.TravelerInfo[i].Gender.ToUpper();
                    if (gender == Sex.MALE.ToString())
                    {
                        pa.Sex = Sex.MALE;

                    }

                    if (gender == Sex.FEMALE.ToString())
                    {
                        pa.Sex = Sex.FEMALE;

                    }
                    pa.FirstName = message.BookFlight.TravelerInfo[i].GivenName;
                    pa.LastName = message.BookFlight.TravelerInfo[i].Surname;
                    pa.Birthday = message.BookFlight.TravelerInfo[i].BirthDate;
                    pa.BaggageCode = "0";
                    pa.PassportNumber = message.BookFlight.TravelerInfo[i]._DocID;
                    pass.PassType = PassType.Passport;
                    pass.IssueDate = message.BookFlight.TravelerInfo[i]._IssuanceDate;
                    pass.IssueCity = message.BookFlight.TravelerInfo[i]._IssuanceCity;
                    pass.ExpiryDate = message.BookFlight.TravelerInfo[i]._ExpireDate;
                    pass.CountryCode = CountryCode.IN;
                    // pass.IssueCity = "NA";
                    pa.PassportDetails = pass;
                    pas[i] = pa;
                    book.PassengerInfo = pas;
                    i++;

                });

                //req1.Username = "Albadie";
                //req1.Password = "01E666EA35";
                req1.Username = message.BookFlight.SupplierAgencyDetails[0].UserName;
                req1.Password = message.BookFlight.SupplierAgencyDetails[0].Password;
                req1.LanguageCode = LanguageCode.EN;
                req1.Request = book;
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12 | SecurityProtocolType.Ssl3;
                try
                {
                    client.PrepareBookFlights(ref req1);
                }

                catch (Exception ex)
                {

                    throw ex;
                }
                string pq = "NIL";
                string AgencyCode = message.BookFlight.SupplierAgencyDetails[0].AgencyCode.ToString();
                string rQ = Newtonsoft.Json.JsonConvert.SerializeObject(req1.Request);
                string rS = Newtonsoft.Json.JsonConvert.SerializeObject(req1.Response);
                BusinessEntities.Common.CustomExtention.SaveLog("BOOKDETAILS", AgencyCode, "PYT001", rQ, rS);


                if (req1.ErrorCode == 0)
                {
                    pq = "NIL";
                    var response = req1.Response;

                    BookFlightsRequest book1 = new BookFlightsRequest();
                    BookFlights req2 = new BookFlights();
                    book1.FlightId = message.BookFlight.Faresourcecode;
                    book1.ConfirmNewPrice = false;
                    //req2.Username = "Albadie";
                    //req2.Password = "01E666EA35";
                    req2.Username = message.BookFlight.SupplierAgencyDetails[0].UserName;
                    req2.Password= message.BookFlight.SupplierAgencyDetails[0].Password;
                    req2.LanguageCode = LanguageCode.EN;
                    req2.Request = book1;

                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12 | SecurityProtocolType.Ssl3;
                    try
                    {
                        client.BookFlights(ref req2);
                    }
                    catch (Exception ex)
                    {

                        throw ex;
                    }

                    var response1 = req2.Response;
                    FLbookRS flbook = new FLbookRS();
                    flbook = PytonbookTOCommonRs(response1);
                    return flbook;

                }
                else
                {
                    pq = req1.ErrorMessage;
                    var response = req1.Response;
                    FLbookRS flbook = new FLbookRS();
                    flbook = PytonpreparebookTOCommonRs(response, pq);
                    return flbook;

                }

            }

            catch (Exception ex)
            {

                throw ex;
            }


        }

        public FLbookRS Bookflight(FLbookRQ message)
        {

            WebServiceSoapClient client = new WebServiceSoapClient();
            FLbookRQ req = new FLbookRQ();
            BookFlightsRequest book = new BookFlightsRequest();
            BookFlights req1 = new BookFlights();
            book.FlightId = message.BookFlight.Faresourcecode;
            book.ConfirmNewPrice = false;
            req1.Username = "Albadie";
            req1.Password = "01E666EA35";
            req1.LanguageCode = LanguageCode.EN;
            req1.Request = book;
            req1.Request = book;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12 | SecurityProtocolType.Ssl3;
            try
            {
                client.BookFlights(ref req1);
            }
            catch (Exception ex)
            {

                throw ex;
            }

            var response = req1.Response;

            FLbookRS flbook = new FLbookRS();
            flbook = PytonbookTOCommonRs(response);
            return flbook;



        }

        public TripDetailsCom_RS Bookflightdetails(TripDetailscomRQ message)
        {
            WebServiceSoapClient client = new WebServiceSoapClient();
            TripDetailscomRQ req = new TripDetailscomRQ();
            GetBookingDetailsRequest book = new GetBookingDetailsRequest();
            GetBookingDetails req1 = new GetBookingDetails();
            book.FlightId = message.UniqueID;

            //req1.Username = "Albadie";
            //req1.Password = "01E666EA35";
            req1.Username = message.SupplierAgencyDetails[0].UserName;
            req1.Password = message.SupplierAgencyDetails[0].Password;
            req1.LanguageCode = LanguageCode.EN;
            req1.Request = book;
           // req1.Request = book;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12 | SecurityProtocolType.Ssl3;
            try
            {
                client.GetBookingDetails(ref req1);
            }
            catch (Exception ex)
            {

                throw ex;
            }

            var response = req1.Response;
            
            string AgencyCode = message.SupplierAgencyDetails[0].AgencyCode.ToString();
            string rQ = Newtonsoft.Json.JsonConvert.SerializeObject(req1.Request);
            string rS = Newtonsoft.Json.JsonConvert.SerializeObject(response);
            BusinessEntities.Common.CustomExtention.SaveLog("TRIPDETAILS", AgencyCode, "PYT001", rQ, rS);
            TripDetailsCom_RS bookdet = new TripDetailsCom_RS();
            bookdet = PytonbookdtTOCommonRs(response);
            return bookdet;

        }

        public async Task<FLcommonRS> FlightSearch(Rootobject message)
        {

            //PytonPartnerServiceReference.WebServiceSoapClient client = new WebServiceSoapClient();

            PytonPartnerServiceReference.WebServiceSoapClient client = new WebServiceSoapClient();

            Origindestinationinformation orgin = new Origindestinationinformation();
            string Year = ""; string Month = ""; string Day = "";
            try
            {
                Year = (message.OriginDestinationInformation[0].DepartureDate).Split('-')[2];
                Month = (message.OriginDestinationInformation[0].DepartureDate).Split('-')[1];
                Day = (message.OriginDestinationInformation[0].DepartureDate).Split('-')[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }
            Passengertypequantity ps = new Passengertypequantity();

            SearchFlightsRequest req = new SearchFlightsRequest();

            req.Departure = message.OriginDestinationInformation[0].OriginLocation;
            req.Destination = message.OriginDestinationInformation[0].DestinationLocation;
            req.DepartureDate = Year + "-" + Month + "-" + Day;
            req.NumADT = message.PassengerTypeQuantity.ADT;
            req.NumINF = message.PassengerTypeQuantity.INF;
            req.NumCHD = message.PassengerTypeQuantity.CHD;
            string adtcount = message.PassengerTypeQuantity.ADT.ToString();
            string chdcount = message.PassengerTypeQuantity.CHD.ToString();
            string infcount = message.PassengerTypeQuantity.INF.ToString();
            req.CurrencyCode = CurrencyCode.EUR;
            req.WaitForResult = true;
            req.NearbyDepartures = false;
            req.NearbyDestinations = false;
            req.RROnly = false;
            req.MetaSearch = false;
            Provider[] prov = new Provider[] {
                 Provider.ElsyArres,
             };
            req.Providers = prov;
            SearchFlights sfl = new SearchFlights();
            sfl.Username = "Albadie";
            sfl.Password = "01E666EA35";
            sfl.LanguageCode = LanguageCode.EN;
            sfl.AppVersion = "";

            sfl.Request = req;

            try
            {
                client.SearchFlights(ref sfl);
            }
            catch (Exception ex)
            {

                throw ex;
            }

            var response = sfl.Response;

            {

            }

            FLcommonRS fLcommonRS = new FLcommonRS();
            fLcommonRS = PytonTOCommonRs(response, adtcount, chdcount, infcount);
            return fLcommonRS;
            //throw new NotImplementedException();

        }



        public FLcommonRS PytonTOCommonRs(SearchFlightsResponse response, string adtcount, string chdcount, string infcount)
        {
            XmlSerializer serializer = new XmlSerializer(response.GetType());

            using (StringWriter writer = new StringWriter())
            {
                serializer.Serialize(writer, response);

                string test = writer.ToString();
            }
            Faremasterpricertravelboardsearchreply faremasterpricertravelboardsearchreply = new Faremasterpricertravelboardsearchreply();
            BusinessEntities.Status stat = new BusinessEntities.Status()
            {
                advisoryTypeInfo = ""

            };
            Replystatus replystatus = new Replystatus();
            FLcommonRS fLcommonRS = new FLcommonRS();
            replystatus.status = stat;
            faremasterpricertravelboardsearchreply.replyStatus = replystatus;
            Conversionrate conversionrate = new Conversionrate();
            Conversionratedetail conversionratedetail = new Conversionratedetail()
            {
                currency = "AED"
            };
            conversionrate.SessionId = "";
            conversionrate.conversionRateDetail = conversionratedetail;
            faremasterpricertravelboardsearchreply.conversionRate = conversionrate;
            int totalflights = response.Flights.Length;
            Flightindex[] Flightindexes = new Flightindex[totalflights];
            Flightindex flightindex = new Flightindex();
            Segmentref segmentref = new Segmentref();
            Typeref typeref = new Typeref();
            Reccount reccount = new Reccount();
            BusinessEntities.Fare fare = new BusinessEntities.Fare();
            Faredetails faredetails = new Faredetails();
            Bagdetails bagdetails = new Bagdetails();
            Description description = new Description();
            Groupofflight groupofflight = new Groupofflight();
            Flightproposal flightproposal = new Flightproposal();
            Flightdetail flightdetail = new Flightdetail();
            Finfo finfo = new Finfo();
            Datetime datetime = new Datetime();
            Location location = new Location();
            Companyid companyid = new Companyid();
            Flightcharacteristics flightcharacteristics = new Flightcharacteristics();
            int resultscound = 1;
            int i = 0;
            foreach (var j in response.Flights)
            {
                // int u= j.Outbound.Legs.Length;
                int count = j.Outbound.Legs.Length;
                int count1 = 0;
                if (j.Inbound != null)
                {
                    count1 = j.Inbound.Legs.Length;
                }
                int count3 = count + count1;
                int totgroups = 0;
                if (response.Roundtrip == true)
                {
                    totgroups = 2;

                }
                else
                {
                    totgroups = 1;
                }
                Groupofflight[] Groupofflights = new Groupofflight[totgroups];
                flightindex = new Flightindex();
                string flightfromkey = j.Outbound.DepCode;
                string flighttokey = j.Outbound.DestCode;
                string flightdepdatekey = j.Outbound.DepDateTime.ToString("dd-MM-yy-HH-mm-ss");
                string yearkey = flightdepdatekey.Split('-')[2];
                string monthkey = flightdepdatekey.Split('-')[1];
                string daykey = flightdepdatekey.Split('-')[0];
                string hourkey = flightdepdatekey.Split('-')[3];
                string minkey = flightdepdatekey.Split('-')[4];
                string codekey = response.Flights[i].Outbound.CarCode;
                string flightnumberkey = j.Outbound.FlightNo;
                string flightkey = flightfromkey + "-" + flighttokey + "-" + daykey + monthkey + yearkey + "-" + hourkey + minkey + "-" + codekey + "-" + flightnumberkey;
                string FareSourceCodeMysti = "";
                segmentref = new Segmentref();
                segmentref.segRef = FareSourceCodeMysti.ToString();
                segmentref.PSessionId = "";
                segmentref.supplier = "PYT001";
                segmentref.key = flightkey;
                flightindex.SegmentRef = segmentref;
                typeref = new Typeref()
                {
                    type = "0"
                };
                flightindex.TypeRef = typeref;
                reccount = new Reccount()
                {
                    rec_id = resultscound.ToString(),
                    combi_id = "2",
                };

                flightindex.reccount = reccount;
                string basefare;
                string taxfare;
                if (response.Roundtrip == true)
                {
                    float basefarecentoutboundadt = float.Parse(j.Outbound.FareADT.ToString()) / 100 * float.Parse(adtcount);
                    float basefarecentoutboundchd = float.Parse(j.Outbound.FareCHD.ToString()) / 100 * float.Parse(chdcount);
                    float basefarecentoutboundinf = float.Parse(j.Outbound.FareINF.ToString()) / 100 * float.Parse(infcount);
                    float basefareoutbound = basefarecentoutboundadt + basefarecentoutboundchd + basefarecentoutboundinf;
                    float basefareinboundcentadt = float.Parse(j.Inbound.FareADT.ToString()) / 100 * float.Parse(adtcount);
                    float basefareinboundchd = float.Parse(j.Inbound.FareCHD.ToString()) / 100 * float.Parse(chdcount);
                    float basefareinboundinf = float.Parse(j.Inbound.FareINF.ToString()) / 100 * float.Parse(infcount);
                    float basefareinbound = basefareinboundcentadt + basefareinboundchd + basefareinboundinf;
                    float basefaretot = basefareinbound + basefareoutbound;
                    float taxfarecentoutbound = float.Parse(j.Outbound.Taxes.ToString()) / 100;
                    float taxfarecentinbound = float.Parse(j.Inbound.Taxes.ToString()) / 100;
                    float taxfarecent = taxfarecentoutbound + taxfarecentinbound;
                    float misfeescentoutbound = float.Parse(j.Outbound.MiscFees.ToString()) / 100;
                    float misfeescentinbound = float.Parse(j.Inbound.MiscFees.ToString()) / 100;
                    float misfeescent = misfeescentoutbound + misfeescentinbound;
                    float tottax = taxfarecent + misfeescent;
                    taxfare = tottax.ToString();
                    basefare = basefaretot.ToString();

                }

                else
                {
                    float basefarecentoutboundadt = float.Parse(j.Outbound.FareADT.ToString()) / 100 * float.Parse(adtcount);
                    float basefarecentoutboundchd = float.Parse(j.Outbound.FareCHD.ToString()) / 100 * float.Parse(chdcount);
                    float basefarecentoutboundinf = float.Parse(j.Outbound.FareINF.ToString()) / 100 * float.Parse(infcount);
                    float basefareoutbound = basefarecentoutboundadt + basefarecentoutboundchd + basefarecentoutboundinf;
                    float taxfarecentoutbound = float.Parse(j.Outbound.Taxes.ToString()) / 100;
                    float misfeescentoutbound = float.Parse(j.Outbound.MiscFees.ToString()) / 100;
                    float tottax = taxfarecentoutbound + misfeescentoutbound;
                    taxfare = tottax.ToString();
                    basefare = basefareoutbound.ToString();

                }
                string currencyCode = response.CurrencyCode.ToString();
                float totalfarecent = float.Parse(j.TotalFare.ToString()) / 100;
                string totalfare = totalfarecent.ToString();
                fare = new BusinessEntities.Fare()
                {
                    amount = totalfare,
                    taxfare = taxfare,
                    basefare = basefare,
                    MarkupFare = "0",
                    currency = currencyCode
                };
                flightindex.fare = fare;
                faredetails = new Faredetails();

                {
                    faredetails.fareBasis = j.FlightId;
                };
                flightindex.fareDetails = faredetails;
                bagdetails = new Bagdetails()
                {
                    freeAllowance = "0",
                    Qcode = "",
                    unit = ""
                };
                flightindex.Bagdetails = bagdetails;
                description = new Description()
                {
                    Pricingmsg = ""
                };
                flightindex.Description = description;
                int totalgroups = 0;
                if (response.Roundtrip == true)
                {
                    totalgroups = 2;

                }

                else
                {
                    totalgroups = 1;

                }
                for (int groups = 0; groups < totalgroups; groups++)
                {
                    if (groups == 0)
                    {
                        groupofflight = new Groupofflight();
                        flightproposal = new Flightproposal()
                        {
                            elapse = j.Outbound.Duration,
                            unitQualifier = j.Outbound.FlightNo,
                            VAirline = j.Outbound.CarCode,
                            Ocarrier = j.Outbound.CarCode,
                        };
                        groupofflight.FlightProposal = flightproposal;
                        Flightdetail[] flightdetails = new Flightdetail[count];
                        for (int fl = 0; fl < count; fl++)
                        {
                            DateTime dataarrdate = j.Outbound.Legs[fl].ArrDateTime;
                            DateTime datadeptdate = j.Outbound.Legs[fl].DepDateTime;
                            TimeSpan ts = dataarrdate - datadeptdate;
                            string formattedTimespan = ts.ToString("hh\\:mm");
                            string AirlegDepdatetime = j.Outbound.Legs[fl].DepDateTime.ToString("dd-MM-yy-HH-mm-ss");
                            string AirlegArrdatetime = j.Outbound.Legs[fl].ArrDateTime.ToString("dd-MM-yy-HH-mm-ss");
                            string AirlegDepYear = AirlegDepdatetime.Split('-')[2];
                            string AirlegDepMonth = AirlegDepdatetime.Split('-')[1];
                            string AirlegDepDay = AirlegDepdatetime.Split('-')[0];
                            string AirlegDepHour = AirlegDepdatetime.Split('-')[3];
                            string AirlegDepMinute = AirlegDepdatetime.Split('-')[4];
                            string AirlegArrYear = AirlegArrdatetime.Split('-')[2];
                            string AirlegArrMonth = AirlegArrdatetime.Split('-')[1];
                            string AirlegArrDay = AirlegArrdatetime.Split('-')[0];
                            string AirlegArrHour = AirlegArrdatetime.Split('-')[3];
                            string AirlegArrMinute = AirlegArrdatetime.Split('-')[4];
                            string AirLegDuration = j.Outbound.Duration;
                            string AirLegElapse = AirLegDuration;
                            flightdetail = new Flightdetail();
                            finfo = new Finfo();
                            datetime = new Datetime()
                            {
                                Depdate = AirlegDepDay + AirlegDepMonth + AirlegDepYear,
                                Deptime = AirlegDepHour + AirlegDepMinute,
                                Arrdate = AirlegArrDay + AirlegArrMonth + AirlegArrYear,
                                Arrtime = AirlegArrHour + AirlegArrMinute,
                                Variation = AirLegElapse
                            };
                            finfo.DateTime = datetime;
                            string AirLegDepFrom = j.Outbound.Legs[fl].DepCode;
                            string AirLegArrFrom = j.Outbound.Legs[fl].DestCode;
                            location = new Location()
                            {
                                locationFrom = AirLegDepFrom,
                                LocationTo = AirLegArrFrom,
                                Fromterminal = "",
                                Toterminal = ""
                            };
                            finfo.location = location;
                            string Mcarriercode = j.Outbound.CarCode;
                            string Ocarriercode = j.Outbound.CarCode;
                            companyid = new Companyid()
                            {
                                mCarrier = Mcarriercode,
                                oCarrier = Ocarriercode
                            };
                            faredetails = new Faredetails();
                            {

                            };

                            finfo.companyId = companyid;
                            string Flnumber = j.Outbound.Legs[fl].FlightNo;
                            string Fleqpment = "";
                            bool Fleticketing = false;
                            string bclass = "Y";
                            BusinessEntities.Enumeration.fareclasstype kh;
                            switch (j.Outbound.Legs[fl].FareClass.ToString())
                            {
                                case "Economy":
                                    bclass = "Y";
                                    break;
                                case "EcoFlex":
                                    bclass = "Y";
                                    break;
                                case "Business":
                                    bclass = "C";
                                    break;
                                case "First":
                                    bclass = "F";
                                    break;
                                default:
                                    bclass = "Y";
                                    break;
                            }
                            finfo.flightNo = Flnumber;
                            finfo.eqpType = Fleqpment;
                            finfo.eTicketing = Fleticketing.ToString();
                            finfo.attributeType = "";
                            finfo.Elapsedtime = formattedTimespan;
                            finfo.BookingClass = bclass;
                            flightdetail.Finfo = finfo;
                            flightcharacteristics = new Flightcharacteristics()
                            {
                                inFlightSrv = ""
                            };

                            flightdetail.flightCharacteristics = flightcharacteristics;
                            flightdetails[fl] = flightdetail;

                            groupofflight.flightDetails = flightdetails;

                        }

                    }

                    else
                    {
                        if (j.Inbound != null)
                        {
                            groupofflight = new Groupofflight();
                            flightproposal = new Flightproposal()
                            {
                                elapse = "",
                                unitQualifier = "",
                                VAirline = j.Inbound.CarCode,
                                Ocarrier = j.Inbound.CarCode,
                            };
                            groupofflight.FlightProposal = flightproposal;
                            Flightdetail[] flightdetails = new Flightdetail[count1];
                            for (int fl = 0; fl < count1; fl++)
                            {
                                DateTime dataarrdate = j.Inbound.Legs[fl].ArrDateTime;
                                DateTime datadeptdate = j.Inbound.Legs[fl].DepDateTime;
                                TimeSpan ts = dataarrdate - datadeptdate;
                                string formattedTimespan = ts.ToString("hh\\:mm");
                                string AirlegDepdatetime = j.Inbound.Legs[fl].DepDateTime.ToString("dd-MM-yy-HH-mm-ss");
                                string AirlegArrdatetime = j.Inbound.Legs[fl].ArrDateTime.ToString("dd-MM-yy-HH-mm-ss");
                                string AirlegDepYear = AirlegDepdatetime.Split('-')[2];
                                string AirlegDepMonth = AirlegDepdatetime.Split('-')[1];
                                string AirlegDepDay = AirlegDepdatetime.Split('-')[0];
                                string AirlegDepHour = AirlegDepdatetime.Split('-')[3];
                                string AirlegDepMinute = AirlegDepdatetime.Split('-')[4];
                                string AirlegArrYear = AirlegArrdatetime.Split('-')[2];
                                string AirlegArrMonth = AirlegArrdatetime.Split('-')[1];
                                string AirlegArrDay = AirlegArrdatetime.Split('-')[0];
                                string AirlegArrHour = AirlegArrdatetime.Split('-')[3];
                                string AirlegArrMinute = AirlegArrdatetime.Split('-')[4];
                                string AirLegDuration = response.Flights[i].Inbound.Duration;
                                string AirLegElapse = AirLegDuration;
                                flightdetail = new Flightdetail();
                                finfo = new Finfo();
                                datetime = new Datetime()
                                {
                                    Depdate = AirlegDepDay + AirlegDepMonth + AirlegDepYear,
                                    Deptime = AirlegDepHour + AirlegDepMinute,
                                    Arrdate = AirlegArrDay + AirlegArrMonth + AirlegArrYear,
                                    Arrtime = AirlegArrHour + AirlegArrMinute,
                                    Variation = AirLegElapse
                                };
                                finfo.DateTime = datetime;
                                string AirLegDepFrom = j.Inbound.Legs[fl].DepCode;
                                string AirLegArrFrom = j.Inbound.Legs[fl].DestCode;
                                location = new Location()
                                {
                                    locationFrom = AirLegDepFrom,
                                    LocationTo = AirLegArrFrom,
                                    Fromterminal = "",
                                    Toterminal = ""
                                };
                                finfo.location = location;
                                string Mcarriercode = j.Inbound.CarCode;
                                string Ocarriercode = j.Inbound.CarCode;
                                companyid = new Companyid()
                                {
                                    mCarrier = Mcarriercode,
                                    oCarrier = Ocarriercode
                                };
                                faredetails = new Faredetails();
                                {

                                };

                                finfo.companyId = companyid;
                                string Flnumber = j.Inbound.Legs[fl].FlightNo;
                                string Fleqpment = "";
                                bool Fleticketing = false;
                                string bclass = "Y";
                                BusinessEntities.Enumeration.fareclasstype kh;
                                switch (j.Inbound.Legs[fl].FareClass.ToString())
                                {
                                    case "Economy":
                                        bclass = "Y";
                                        break;
                                    case "EcoFlex":
                                        bclass = "Y";
                                        break;
                                    case "Business":
                                        bclass = "C";
                                        break;
                                    case "First":
                                        bclass = "F";
                                        break;
                                    default:
                                        bclass = "Y";
                                        break;

                                }

                                finfo.flightNo = Flnumber;
                                finfo.eqpType = Fleqpment;
                                finfo.eTicketing = Fleticketing.ToString();
                                finfo.attributeType = "";
                                finfo.Elapsedtime = formattedTimespan;
                                finfo.BookingClass = bclass;
                                flightdetail.Finfo = finfo;
                                flightcharacteristics = new Flightcharacteristics()
                                {
                                    inFlightSrv = ""
                                };

                                flightdetail.flightCharacteristics = flightcharacteristics;
                                flightdetails[fl] = flightdetail;

                                groupofflight.flightDetails = flightdetails;
                            }

                        }
                    }
                    Groupofflights[groups] = groupofflight;

                }
                flightindex.groupOfFlights = Groupofflights;
                Flightindexes[i] = flightindex;
                resultscound++;
                i++;
            }
            faremasterpricertravelboardsearchreply.flightIndex = Flightindexes;
            fLcommonRS.fareMasterPricerTravelBoardSearchReply = faremasterpricertravelboardsearchreply;
            return fLcommonRS;

        }

        public FareRevalidateRS PytonValidateTOCommonRs(GetFlightDetailsResponse response, string faresource,string carddt)
        {
            try
            {
                FareRevalidateRS fLcommonRS = new FareRevalidateRS();
                float totalfarecent = float.Parse(response.FlightDetails.TotalFare.ToString()) / 100;
                string totalfare = totalfarecent.ToString();
                string basefare;
                string taxfare;
                string currencycode = response.CurrencyCode.ToString();
                int totalallsegments1 = 0;
                if (response.FlightDetails.Roundtrip == true)
                {
                    float taxfarecentoutbound = float.Parse(response.FlightDetails.Outbound.Taxes.ToString()) / 100;
                    float taxfarecentinbound = float.Parse(response.FlightDetails.Inbound.Taxes.ToString()) / 100;
                    float miscoutbound = float.Parse(response.FlightDetails.Outbound.MiscFees.ToString()) / 100;
                    float misinbound = float.Parse(response.FlightDetails.Inbound.MiscFees.ToString()) / 100;
                    float misc = miscoutbound + misinbound;
                    float taxfarecent = taxfarecentinbound + taxfarecentoutbound;
                    float tottax = misc + taxfarecent;
                    taxfare = tottax.ToString();
                    float basefarecent = totalfarecent - tottax;
                    basefare = basefarecent.ToString();

                }
                else
                {
                    float taxfarecentoutbound = float.Parse(response.FlightDetails.Outbound.Taxes.ToString()) / 100;
                    float miscoutbound = float.Parse(response.FlightDetails.Outbound.MiscFees.ToString()) / 100;
                    float tottax = taxfarecentoutbound + miscoutbound;
                    taxfare = tottax.ToString();
                    float basefarecent = totalfarecent - tottax;
                    basefare = basefarecent.ToString();
                }

                int nadults = response.SearchFlightParams.NumADT;
                int nchild = response.SearchFlightParams.NumCHD;
                int ninfant = response.SearchFlightParams.NumINF;
                int totpax = 1;
                if (nchild > 0)
                {
                    if (ninfant > 0)
                    {
                        totpax = 3;
                    }
                    else
                    {
                        totpax = 2;
                    }
                }

                else
                {
                    if (ninfant > 0)
                    {
                        totpax = 2;
                    }

                    else
                    {
                        totpax = 1;
                    }
                }

                int totalpax = nadults + nchild + ninfant;
                FareRevalidateRS fareRevalidateRS = new FareRevalidateRS();
                Fareinformationwithoutpnrreply fareinformationwithoutpnrreply = new Fareinformationwithoutpnrreply();
                fareinformationwithoutpnrreply.FareSessionid = "";
                fareinformationwithoutpnrreply.Farefaresourcecode = faresource;
                fareinformationwithoutpnrreply.Fareagencycode = "ALBADIE";
                fareinformationwithoutpnrreply.Fsupplier = "PYT001";
                float surchargecent;
                //if (carddt == "AIRPLUS")
                //{ 
                //long fee = response.FlightDetails.PaymentOptions.Where(_ => _.PaymentCode == PaymentCode.AIRPLUS).FirstOrDefault().Fee;
                //surchargecent = fee / 100;
                //}

                //else

                //{
                //    long fee = response.FlightDetails.PaymentOptions.Where(_ => _.PaymentCode == PaymentCode.AIRPLUS).FirstOrDefault().Fee;
                //    surchargecent = fee / 100;
                //}
                long fee;
                switch (carddt)
                {
                    case "AIRPLUS":
                         fee = response.FlightDetails.PaymentOptions.Where(_ => _.PaymentCode == PaymentCode.AIRPLUS).FirstOrDefault().Fee;
                        surchargecent = fee / 100;
                        break;
                    case "AMEX":
                        fee = response.FlightDetails.PaymentOptions.Where(_ => _.PaymentCode == PaymentCode.AMEX).FirstOrDefault().Fee;
                        surchargecent = fee / 100;
                        break;
                    case "CARTEBLEU":
                        fee = response.FlightDetails.PaymentOptions.Where(_ => _.PaymentCode == PaymentCode.CARTEBLEU).FirstOrDefault().Fee;
                        surchargecent = fee / 100;
                        break;
                    case "CBC":
                        fee = response.FlightDetails.PaymentOptions.Where(_ => _.PaymentCode == PaymentCode.CBC).FirstOrDefault().Fee;
                        surchargecent = fee / 100;
                        break;
                    case "DINERS":
                        fee = response.FlightDetails.PaymentOptions.Where(_ => _.PaymentCode == PaymentCode.DINERS).FirstOrDefault().Fee;
                        surchargecent = fee / 100;
                        break;
                    case "DIRECTEBANKING":
                        fee = response.FlightDetails.PaymentOptions.Where(_ => _.PaymentCode == PaymentCode.DIRECTEBANKING).FirstOrDefault().Fee;
                        surchargecent = fee / 100;
                        break;
                    case "ELECTRON":
                        fee = response.FlightDetails.PaymentOptions.Where(_ => _.PaymentCode == PaymentCode.ELECTRON).FirstOrDefault().Fee;
                        surchargecent = fee / 100;
                        break;
                    case "ELSYARRES":
                        fee = response.FlightDetails.PaymentOptions.Where(_ => _.PaymentCode == PaymentCode.ELSYARRES).FirstOrDefault().Fee;
                        surchargecent = fee / 100;
                        break;
                    case "ELV":
                        fee = response.FlightDetails.PaymentOptions.Where(_ => _.PaymentCode == PaymentCode.ELV).FirstOrDefault().Fee;
                        surchargecent = fee / 100;
                        break;
                    case "IDEAL":
                        fee = response.FlightDetails.PaymentOptions.Where(_ => _.PaymentCode == PaymentCode.IDEAL).FirstOrDefault().Fee;
                        surchargecent = fee / 100;
                        break;
                    case "JCB":
                        fee = response.FlightDetails.PaymentOptions.Where(_ => _.PaymentCode == PaymentCode.JCB).FirstOrDefault().Fee;
                        surchargecent = fee / 100;
                        break;
                    case "KBC":
                        fee = response.FlightDetails.PaymentOptions.Where(_ => _.PaymentCode == PaymentCode.KBC).FirstOrDefault().Fee;
                        surchargecent = fee / 100;
                        break;
                    case "MAESTRO":
                        fee = response.FlightDetails.PaymentOptions.Where(_ => _.PaymentCode == PaymentCode.MAESTRO).FirstOrDefault().Fee;
                        surchargecent = fee / 100;
                        break;
                    case "MASTERCARD":
                        fee = response.FlightDetails.PaymentOptions.Where(_ => _.PaymentCode == PaymentCode.MASTERCARD).FirstOrDefault().Fee;
                        surchargecent = fee / 100;
                        break;
                    case "MASTERDEBIT":
                        fee = response.FlightDetails.PaymentOptions.Where(_ => _.PaymentCode == PaymentCode.MASTERDEBIT).FirstOrDefault().Fee;
                        surchargecent = fee / 100;
                        break;
                    case "MASTERPREPAID":
                        fee = response.FlightDetails.PaymentOptions.Where(_ => _.PaymentCode == PaymentCode.MASTERPREPAID).FirstOrDefault().Fee;
                        surchargecent = fee / 100;
                        break;
                    case "UATP)":
                        fee = response.FlightDetails.PaymentOptions.Where(_ => _.PaymentCode == PaymentCode.UATP).FirstOrDefault().Fee;
                        surchargecent = fee / 100;
                        break;
                    case "VISA)":
                        fee = response.FlightDetails.PaymentOptions.Where(_ => _.PaymentCode == PaymentCode.VISA).FirstOrDefault().Fee;
                        surchargecent = fee / 100;
                        break;
                    case "VISADEBIT)":
                        fee = response.FlightDetails.PaymentOptions.Where(_ => _.PaymentCode == PaymentCode.VISADEBIT).FirstOrDefault().Fee;
                        surchargecent = fee / 100;
                        break;
                    default:
                        fee = response.FlightDetails.PaymentOptions.Where(_ => _.PaymentCode == PaymentCode.VISA).FirstOrDefault().Fee;
                        surchargecent = fee / 100;
                        break;

                }

                Flightfaredetails flightdetail = new Flightfaredetails()
                {
                    TotalPax = totalpax.ToString(),
                    TotalFare = totalfare,
                    Basefare = basefare,
                    TaxFare = taxfare,
                    currency = currencycode

                };

                Surchargesgroup surchargesgroup = new Surchargesgroup()
                {
                    Amount = surchargecent.ToString(),
                    countryCode = ""

                };
                flightdetail.surchargesGroup = surchargesgroup;
                fareinformationwithoutpnrreply.FlightFareDetails = flightdetail;
                Paxfaredetail paxfaredetail = new Paxfaredetail();
                Fareinformation fareinformation = new Fareinformation();
                Paxfaredetail[] paxfaredetails = new Paxfaredetail[totpax];
                Surcharges surcharges = new Surcharges();
                for (int FLpaxes = 0; FLpaxes < totpax; FLpaxes++)
                {
                    if (FLpaxes == 0)
                    {
                        int k = response.SearchFlightParams.NumADT;

                        string totalpaxfare;
                        string basepaxfare;
                        string tax;
                        string totalpaxbyunit = response.SearchFlightParams.NumADT.ToString();
                        string totaltype = "ADT";
                        if (response.FlightDetails.Roundtrip == true)
                        {
                            float tax1centoutbound = float.Parse(response.FlightDetails.Outbound.Taxes.ToString()) / 100;
                            float tax1centinbound = float.Parse(response.FlightDetails.Inbound.Taxes.ToString()) / 100;
                            float misccentoutbound = float.Parse(response.FlightDetails.Outbound.MiscFees.ToString()) / 100;
                            float misccentinbound = float.Parse(response.FlightDetails.Inbound.MiscFees.ToString()) / 100;
                            float tottaxcent = tax1centoutbound + tax1centinbound;
                            float totmisc = misccentoutbound + misccentinbound;
                            float tottax = tottaxcent + totmisc;
                            float tax1 = tottax / totalpax;
                            float totalpaxfare1centoutbound = float.Parse(response.FlightDetails.Outbound.FareADT.ToString()) / 100;
                            float totalpaxfare1centinbound = float.Parse(response.FlightDetails.Inbound.FareADT.ToString()) / 100;
                            float totalpaxfare1 = totalpaxfare1centoutbound + totalpaxfare1centinbound + tax1;
                            float basefire1centoutbound = float.Parse(response.FlightDetails.Outbound.FareADT.ToString()) / 100;
                            float basefire1centinbound = float.Parse(response.FlightDetails.Inbound.FareADT.ToString()) / 100;
                            float basefire1cent = basefire1centoutbound + basefire1centinbound;
                            float basefire1 = basefire1cent;
                            totalpaxfare = totalpaxfare1.ToString();
                            // string paxcurrencycode = response.CurrencyCode.ToString();
                            basepaxfare = basefire1.ToString();
                            tax = tax1.ToString();
                        }
                        else
                        {
                            float tax1centoutbound = float.Parse(response.FlightDetails.Outbound.Taxes.ToString()) / 100;
                            float misccentoutbound = float.Parse(response.FlightDetails.Outbound.MiscFees.ToString()) / 100;
                            float tottax = tax1centoutbound + misccentoutbound;
                            float tax1 = tottax / totalpax;
                            float totalpaxfare1cent = float.Parse(response.FlightDetails.Outbound.FareADT.ToString()) / 100;
                            float totalpaxfare1 = totalpaxfare1cent + tax1;
                            float basefire1centoutbound = float.Parse(response.FlightDetails.Outbound.FareADT.ToString()) / 100;
                            float basefire1cent = basefire1centoutbound;
                            float basefire1 = basefire1cent;
                            totalpaxfare = totalpaxfare1.ToString();
                            basepaxfare = basefire1.ToString();
                            tax = tax1.ToString();
                        }

                        string paxcurrencycode = response.CurrencyCode.ToString();
                        paxfaredetail = new Paxfaredetail()
                        {
                            TotalPax = totalpax.ToString(),
                            TotalPaxUnites = totalpaxbyunit.ToString(),
                            PaxType = totaltype,
                            TotalFare = totalpaxfare,
                            Basefare = basepaxfare,
                            TaxFare = tax,
                            currency = paxcurrencycode,
                        };

                        fareinformation = new Fareinformation()
                        {
                            TotalFare = totalpaxfare,
                            Basefare = basepaxfare,
                            TaxFare = tax,
                            currency = paxcurrencycode,
                        };
                        surcharges = new Surcharges()
                        {
                            Amount = "0",
                            countryCode = ""
                        };
                        paxfaredetail.surcharges = surcharges;
                        paxfaredetails[FLpaxes] = paxfaredetail;

                    }

                    else if (FLpaxes == 1)
                    {
                        int k = response.SearchFlightParams.NumCHD;
                        if (k == 0)
                        {
                            int t = response.SearchFlightParams.NumINF;
                            string totalpaxfare;
                            string basepaxfare;
                            string tax;
                            string totalpaxbyunit = response.SearchFlightParams.NumINF.ToString();
                            string totaltype = "INF";
                            if (response.FlightDetails.Roundtrip == true)
                            {
                                float tax1centoutbound = float.Parse(response.FlightDetails.Outbound.Taxes.ToString()) / 100;
                                float tax1centinbound = float.Parse(response.FlightDetails.Inbound.Taxes.ToString()) / 100;
                                float misccentoutbound = float.Parse(response.FlightDetails.Outbound.MiscFees.ToString()) / 100;
                                float misccentinbound = float.Parse(response.FlightDetails.Inbound.MiscFees.ToString()) / 100;
                                float tottaxcent = tax1centoutbound + tax1centinbound;
                                float totmisc = misccentoutbound + misccentinbound;
                                float tottax = tottaxcent + totmisc;
                                float tax1 = tottax / totalpax;
                                float totalpaxfare1centoutbound = float.Parse(response.FlightDetails.Outbound.FareINF.ToString()) / 100;
                                float totalpaxfare1centinbound = float.Parse(response.FlightDetails.Inbound.FareINF.ToString()) / 100;
                                float totalpaxfare1 = totalpaxfare1centoutbound + totalpaxfare1centinbound + tax1;
                                float basefire1centoutbound = float.Parse(response.FlightDetails.Outbound.FareINF.ToString()) / 100;
                                float basefire1centinbound = float.Parse(response.FlightDetails.Inbound.FareINF.ToString()) / 100;
                                float basefire1cent = basefire1centoutbound + basefire1centinbound;
                                float basefire1 = basefire1cent;
                                totalpaxfare = totalpaxfare1.ToString();
                                basepaxfare = basefire1.ToString();
                                tax = tax1.ToString();

                            }
                            else
                            {
                                float tax1centoutbound = float.Parse(response.FlightDetails.Outbound.Taxes.ToString()) / 100;
                                float misccentoutbound = float.Parse(response.FlightDetails.Outbound.MiscFees.ToString()) / 100;
                                float tottax = tax1centoutbound + misccentoutbound;
                                float tax1 = tottax / totalpax;
                                float totalpaxfare1cent = float.Parse(response.FlightDetails.Outbound.FareINF.ToString()) / 100;
                                float totalpaxfare1 = totalpaxfare1cent + tax1;
                                float basefire1centoutbound = float.Parse(response.FlightDetails.Outbound.FareINF.ToString()) / 100;
                                float basefire1cent = basefire1centoutbound;
                                float basefire1 = basefire1cent;
                                totalpaxfare = totalpaxfare1.ToString();
                                basepaxfare = basefire1.ToString();
                                tax = tax1.ToString();

                            }
                            string paxcurrencycode = response.CurrencyCode.ToString();
                            paxfaredetail = new Paxfaredetail()
                            {
                                TotalPax = totalpax.ToString(),
                                TotalPaxUnites = totalpaxbyunit.ToString(),
                                PaxType = totaltype,
                                TotalFare = totalpaxfare,
                                Basefare = basepaxfare,
                                TaxFare = tax,
                                currency = paxcurrencycode
                            };
                            fareinformation = new Fareinformation()
                            {
                                TotalFare = totalpaxfare,
                                Basefare = basepaxfare,
                                TaxFare = tax,
                                currency = paxcurrencycode,
                            };
                            surcharges = new Surcharges()
                            {
                                Amount = "0",
                                countryCode = ""
                            };
                            fareinformation.surcharges = surcharges;
                            paxfaredetail.surcharges = surcharges;
                            paxfaredetails[FLpaxes] = paxfaredetail;
                        }

                        else
                        {
                            string totalpaxfare;
                            string basepaxfare;
                            string tax;
                            string totalpaxbyunit = response.SearchFlightParams.NumCHD.ToString();
                            string totaltype = "CHD";
                            if (response.FlightDetails.Roundtrip == true)
                            {
                                float tax1centoutbound = float.Parse(response.FlightDetails.Outbound.Taxes.ToString()) / 100;
                                float tax1centinbound = float.Parse(response.FlightDetails.Inbound.Taxes.ToString()) / 100;
                                float misccentoutbound = float.Parse(response.FlightDetails.Outbound.MiscFees.ToString()) / 100;
                                float misccentinbound = float.Parse(response.FlightDetails.Inbound.MiscFees.ToString()) / 100;
                                float tottaxcent = tax1centoutbound + tax1centinbound;
                                float totmisc = misccentoutbound + misccentinbound;
                                float tottax = tottaxcent + totmisc;
                                float tax1 = tottax / totalpax;
                                float totalpaxfare1centoutbound = float.Parse(response.FlightDetails.Outbound.FareCHD.ToString()) / 100;
                                float totalpaxfare1centinbound = float.Parse(response.FlightDetails.Inbound.FareCHD.ToString()) / 100;
                                float totalpaxfare1 = totalpaxfare1centoutbound + totalpaxfare1centinbound + tax1;
                                float basefire1centoutbound = float.Parse(response.FlightDetails.Outbound.FareCHD.ToString()) / 100;
                                float basefire1centinbound = float.Parse(response.FlightDetails.Inbound.FareCHD.ToString()) / 100;
                                float basefire1cent = basefire1centoutbound + basefire1centinbound;
                                float basefire1 = basefire1cent;
                                totalpaxfare = totalpaxfare1.ToString();
                                basepaxfare = basefire1.ToString();
                                tax = tax1.ToString();
                            }
                            else
                            {
                                float tax1centoutbound = float.Parse(response.FlightDetails.Outbound.Taxes.ToString()) / 100;
                                float misccentoutbound = float.Parse(response.FlightDetails.Outbound.MiscFees.ToString()) / 100;
                                float tottax = tax1centoutbound + misccentoutbound;
                                float tax1 = tottax / totalpax;
                                float totalpaxfare1cent = float.Parse(response.FlightDetails.Outbound.FareCHD.ToString()) / 100;
                                float totalpaxfare1 = totalpaxfare1cent + tax1;
                                float basefire1centoutbound = float.Parse(response.FlightDetails.Outbound.FareCHD.ToString()) / 100;
                                float basefire1cent = basefire1centoutbound;
                                float basefire1 = basefire1cent;
                                totalpaxfare = totalpaxfare1.ToString();
                                basepaxfare = basefire1.ToString();
                                tax = tax1.ToString();
                            }
                            string paxcurrencycode = response.CurrencyCode.ToString();
                            fareinformation = new Fareinformation()
                            {
                                TotalFare = totalpaxfare,
                                Basefare = basepaxfare,
                                TaxFare = tax,
                                currency = paxcurrencycode,
                            };
                            paxfaredetail = new Paxfaredetail()
                            {
                                TotalPax = totalpax.ToString(),
                                TotalPaxUnites = totalpaxbyunit.ToString(),
                                PaxType = totaltype,
                                TotalFare = totalpaxfare,
                                Basefare = basepaxfare,
                                TaxFare = tax,
                                currency = paxcurrencycode,
                            };
                            surcharges = new Surcharges()
                            {
                                Amount = "0",
                                countryCode = ""
                            };
                            paxfaredetail.surcharges = surcharges;
                            paxfaredetails[FLpaxes] = paxfaredetail;
                        }
                    }

                    else
                    {
                        int k = response.SearchFlightParams.NumINF;

                        string totalpaxfare;
                        string basepaxfare;
                        string tax;
                        string totalpaxbyunit = response.SearchFlightParams.NumINF.ToString();
                        string totaltype = "INF";
                        if (response.FlightDetails.Roundtrip == true)
                        {
                            float tax1centoutbound = float.Parse(response.FlightDetails.Outbound.Taxes.ToString()) / 100;
                            float tax1centinbound = float.Parse(response.FlightDetails.Inbound.Taxes.ToString()) / 100;
                            float misccentoutbound = float.Parse(response.FlightDetails.Outbound.MiscFees.ToString()) / 100;
                            float misccentinbound = float.Parse(response.FlightDetails.Inbound.MiscFees.ToString()) / 100;
                            float tottaxcent = tax1centoutbound + tax1centinbound;
                            float totmisc = misccentoutbound + misccentinbound;
                            float tottax = tottaxcent + totmisc;
                            float tax1 = tottax / totalpax;
                            float totalpaxfare1centoutbound = float.Parse(response.FlightDetails.Outbound.FareINF.ToString()) / 100;
                            float totalpaxfare1centinbound = float.Parse(response.FlightDetails.Inbound.FareINF.ToString()) / 100;
                            float totalpaxfare1 = totalpaxfare1centoutbound+ totalpaxfare1centinbound + tax1;
                            float basefire1centoutbound = float.Parse(response.FlightDetails.Outbound.FareINF.ToString()) / 100;
                            float basefire1centinbound = float.Parse(response.FlightDetails.Inbound.FareINF.ToString()) / 100;
                            float basefire1cent = basefire1centoutbound + basefire1centinbound;
                            float basefire1 = basefire1cent;
                            totalpaxfare = totalpaxfare1.ToString();
                            basepaxfare = basefire1.ToString();
                            tax = tax1.ToString();

                        }
                        else
                        {
                            float tax1centoutbound = float.Parse(response.FlightDetails.Outbound.Taxes.ToString()) / 100;
                            float misccentoutbound = float.Parse(response.FlightDetails.Outbound.MiscFees.ToString()) / 100;
                            float tottax = tax1centoutbound + misccentoutbound;
                            float tax1 = tottax / totalpax;
                            float totalpaxfare1cent = float.Parse(response.FlightDetails.Outbound.FareINF.ToString()) / 100;
                            float totalpaxfare1 = totalpaxfare1cent + tax1;
                            float basefire1centoutbound = float.Parse(response.FlightDetails.Outbound.FareINF.ToString()) / 100;

                            float basefire1cent = basefire1centoutbound;
                            float basefire1 = basefire1cent;
                            totalpaxfare = totalpaxfare1.ToString();
                            basepaxfare = basefire1.ToString();
                            tax = tax1.ToString();

                        }
                        string paxcurrencycode = response.CurrencyCode.ToString();
                        paxfaredetail = new Paxfaredetail()
                        {
                            TotalPax = totalpax.ToString(),
                            TotalPaxUnites = totalpaxbyunit.ToString(),
                            PaxType = totaltype,
                            TotalFare = totalpaxfare,
                            Basefare = basepaxfare,
                            TaxFare = tax,
                            currency = paxcurrencycode
                        };
                        fareinformation = new Fareinformation()
                        {
                            TotalFare = totalpaxfare,
                            Basefare = basepaxfare,
                            TaxFare = tax,
                            currency = paxcurrencycode,
                        };
                        surcharges = new Surcharges()
                        {
                            Amount = "0",
                            countryCode = ""
                        };
                        fareinformation.surcharges = surcharges;
                        paxfaredetail.surcharges = surcharges;
                        // paxfaredetails[i] = paxfaredetail;
                        paxfaredetails[FLpaxes] = paxfaredetail;
                    }
                }


                fareinformationwithoutpnrreply.PaxFareDetails = paxfaredetails;
                int totalleg = 0;
                int totalallsegments = 0;
                int airsegmentsIndex = response.FlightDetails.Inbound != null
                                    ? (response.FlightDetails.Inbound.Legs.Length + response.FlightDetails.Outbound.Legs.Length)
                                    : response.FlightDetails.Outbound.Legs.Length;
                Airsegment[] airsegments = new Airsegment[airsegmentsIndex];//{ };//new Airsegment[] { };
                int segflag = 0;


                if (response.FlightDetails.Roundtrip == true)
                {
                    totalallsegments1 = 2;
                }
                else
                {
                    totalallsegments1 = 1;
                }
                Airsegment airsegment = new Airsegment();
                Seginfo seginfo = new Seginfo();
                Fldata fldata = new Fldata();
                Dateandtimedetails dateandtimedetails = new Dateandtimedetails();
                Cabingroup cabingroup = new Cabingroup();
                Baggageallowance baggageallowance = new Baggageallowance();
                // Baggageinformation baggageinfo = new Baggageinformation();
                Fareerror fare = new Fareerror();
                int segdt = 0;
                for (int FLseg = 0; FLseg < totalallsegments1; FLseg++)
                {

                    if (FLseg == 1)
                    {
                        int legcount = response.FlightDetails.Inbound.Legs.Length;
                        
                        foreach (var k in response.FlightDetails.Inbound.Legs)
                        {
                            string bclass = "Y";
                            switch (k.FareClass.ToString())
                            {
                                case "Economy":
                                    bclass = "Y";
                                    break;
                                case "EcoFlex":
                                    bclass = "Y";
                                    break;
                                case "Business":
                                    bclass = "C";
                                    break;
                                case "First":
                                    bclass = "F";
                                    break;
                                default:
                                    bclass = "Y";
                                    break;

                            }
                            airsegment = new Airsegment();
                            seginfo = new Seginfo();
                            DateTime dataarrdate = k.DepDateTime;

                            DateTime datadeptdate = k.ArrDateTime;

                            TimeSpan ts = dataarrdate - datadeptdate;

                            string formattedTimespan = ts.ToString("hh\\:mm");
                            fldata = new Fldata()
                            {
                                BoardPoint = k.DepCode,
                                BoardTerminal = "",
                                Offpoint = k.DestCode,
                                OffTerminal = "",
                                FlightNumber = k.FlightNo,
                                BookingClass = bclass,
                                MarketingCompany = k.CarCode,
                                OperatingCompany = k.CarCode,
                                Equiptype = k.CarCode,
                                durationtime = formattedTimespan
                            };
                            seginfo.FLData = fldata;
                            dateandtimedetails = new Dateandtimedetails()
                            {
                                DepartureDate = k.DepDateTime.ToString("ddMMyy"),
                                DepartureTime = k.DepDateTime.ToString("HHmm"),
                                ArrivalDate = k.ArrDateTime.ToString("ddMMyy"),
                                ArrivalTime = k.ArrDateTime.ToString("HHmm")
                            };
                            seginfo.DateandTimeDetails = dateandtimedetails;
                            airsegment.FareSourceCode = faresource;
                            cabingroup = new Cabingroup()
                            {
                                designator = ""
                            };
                            airsegment.CabinGroup = cabingroup;
                            Baggageinformation baggageinfo = new Baggageinformation()
                            {
                                BagPaxType = "ADT",
                                Baggage = response.FlightDetails.BaggageOptions[0].Weight.ToString(),
                                // Baggage = "NA",
                                Unit = "KG"
                            };
                            Baggageinformation[] bag = new Baggageinformation[] { baggageinfo };
                            airsegment.BaggageInformations = bag;
                            airsegment.SegInfo = seginfo;
                            airsegments[segdt] = airsegment;
                            airsegments[segdt].SegDirectionID = 1;
                            airsegments[segdt].OriginCity = k.DestCode;
                            airsegments[segdt].DepartureCity = k.DepCode;
                            //segflag++;
                            segdt++;
                        }
                    }

                    else
                    {
                        int legcount = response.FlightDetails.Outbound.Legs.Length;
                       
                        foreach (var k in response.FlightDetails.Outbound.Legs)
                        {
                            DateTime dataarrdate = k.DepDateTime;

                            DateTime datadeptdate = k.ArrDateTime;

                            TimeSpan ts = dataarrdate - datadeptdate;

                            string formattedTimespan = ts.ToString("hh\\:mm");

                            string bclass = "Y";
                            switch (k.FareClass.ToString())
                            {
                                case "Economy":
                                    bclass = "Y";
                                    break;
                                case "EcoFlex":
                                    bclass = "Y";
                                    break;
                                case "Business":
                                    bclass = "C";
                                    break;
                                case "First":
                                    bclass = "F";
                                    break;
                                default:
                                    bclass = "Y";
                                    break;

                            }

                            airsegment = new Airsegment();
                            seginfo = new Seginfo();
                            fldata = new Fldata()
                            {
                                BoardPoint = k.DepCode,
                                BoardTerminal = "",
                                Offpoint = k.DestCode,
                                OffTerminal = "",
                                FlightNumber = k.FlightNo,
                                BookingClass = bclass,
                                MarketingCompany = k.CarCode,
                                OperatingCompany = k.CarCode,
                                Equiptype = k.CarCode,
                                durationtime = formattedTimespan
                            };
                            seginfo.FLData = fldata;
                            dateandtimedetails = new Dateandtimedetails()
                            {
                                DepartureDate = k.DepDateTime.ToString("ddMMyy"),
                                DepartureTime = k.DepDateTime.ToString("HHmm"),
                                ArrivalDate = k.ArrDateTime.ToString("ddMMyy"),
                                ArrivalTime = k.ArrDateTime.ToString("HHmm")
                            };
                            seginfo.DateandTimeDetails = dateandtimedetails;
                            airsegment.FareSourceCode = "";
                            cabingroup = new Cabingroup()
                            {
                                designator = ""
                            };
                            airsegment.CabinGroup = cabingroup;
                          
                            Baggageinformation baggageinfo = new Baggageinformation()
                            {
                                BagPaxType = "ADT",
                                Baggage = response.FlightDetails.BaggageOptions[0].Weight.ToString(),


                                // Baggage = "NA",
                                Unit = "KG"
                            };
                            Baggageinformation[] bag = new Baggageinformation[] { baggageinfo };
                            airsegment.BaggageInformations = bag;
                            airsegment.SegInfo = seginfo;
                            airsegments[segdt] = airsegment;
                            airsegments[segdt].SegDirectionID = 0;
                            airsegments[segdt].OriginCity = k.DestCode;
                            airsegments[segdt].DepartureCity = k.DepCode;
                            //segflag++;
                            segdt++;
                        }
                    }
                }

                //Segments end here
                fareinformationwithoutpnrreply.AirSegments = airsegments;

                fare = new Fareerror()
                {
                    code = "",
                    errormessage = ""
                };
                Fareerror[] fareErrors = new Fareerror[] { fare };

                fareinformationwithoutpnrreply.FareErrors = fareErrors;

                // Airfarerule[] airfares = new Airfarerule[2];

                Airfarerule airfare = new Airfarerule()
                {
                    Airline = response.FlightDetails.Outbound.CarCode,
                    Arrival = response.FlightDetails.Outbound.DestCode,
                    Departure = response.FlightDetails.Outbound.DepCode

                };

                int count = response.FlightDetails.Outbound.FareInfo.Length;
                if (count > 0)
                {
                    Fareruledetail[] farerules = new Fareruledetail[count];
                    for (int k = 0; k < count; k++)
                    {
                        Fareruledetail farerule = new Fareruledetail()
                        {
                            Rulehead = "Rules",
                            RuleBody = response.FlightDetails.Outbound.FareInfo[k]


                        };
                        farerules[k] = farerule;

                    }
                    airfare.FareRuleDetails = farerules;
                    Airfarerule[] airfares = new Airfarerule[] { airfare };
                    //Airfarerule[] airfares = new Airfarerule[] { airfare };
                    fareinformationwithoutpnrreply.AirFareRule = airfares;
                }

                else
                {

                    Airfarerule[] airfares = new Airfarerule[0];
                    // Airfarerule[] airfares = new Airfarerule[] { airfare };
                    fareinformationwithoutpnrreply.AirFareRule = airfares;

                }

                fareinformationwithoutpnrreply.Success = "true";
                fareinformationwithoutpnrreply.Target = "";
                fareinformationwithoutpnrreply.SupplierOGPrice = totalfare;
                fareRevalidateRS.FareInformationWithoutPNRReply = fareinformationwithoutpnrreply;
                return fareRevalidateRS;

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public FLbookRS PytonbookTOCommonRs(BookFlightsResponse response)
        {
            FLbookRS flbook = new FLbookRS();

            if (response.Booked == true)
            {
                if (response.BookFlightsResult.Outbound.BookingStatus.ToString() == "Confirmed")
                {
                    BusinessEntities.Bookflightresult book = new BusinessEntities.Bookflightresult()
                    {
                        Status = "OK",
                        Success = "true",
                        Target = "",
                        TktTimeLimit = "",
                        UniqueID = response.BookFlightsResult.Outbound.BookingCode,
                        Airlinepnr = response.BookFlightsResult.Outbound.BookingCode,
                        Sessionid = "",
                        Faresourcecode = "",
                        AgencyCode = "Albadie"

                    };

                    Error err = new Error()
                    {
                        Code = "0",
                        Message = "NIL"

                    };

                    Error[] errors = new Error[] { err };

                    book.Errors = errors;

                    flbook.BookFlightResult = book;

                }

                else
                {
                    BusinessEntities.Bookflightresult book = new BusinessEntities.Bookflightresult()
                    {
                        Status = "HK",
                        Success = "true",
                        Target = "",
                        TktTimeLimit = "",
                        UniqueID = response.BookFlightsResult.Outbound.BookingCode,
                        Airlinepnr = response.BookFlightsResult.Outbound.BookingCode,
                        Sessionid = "",
                        Faresourcecode = "",
                        AgencyCode = "Albadie"

                    };

                    Error err = new Error()
                    {
                        Code = "0",
                        Message = "NIL"

                    };

                    Error[] errors = new Error[] { err };

                    book.Errors = errors;

                    flbook.BookFlightResult = book;
                }
            }

            else
            {
                BusinessEntities.Bookflightresult book = new BusinessEntities.Bookflightresult()
                {

                    Status = "pending",
                    Success = "false",
                    Target = "",
                    TktTimeLimit = "",
                    UniqueID = "NIL",
                    Airlinepnr = "NIL",
                    Sessionid = "",
                    Faresourcecode = "",
                    AgencyCode = ""

                };

                Error err = new Error()
                {
                    Code = "0",
                    Message = "NIL"

                };

                Error[] errors = new Error[] { err };

                book.Errors = errors;

                flbook.BookFlightResult = book;

            }

            return flbook;

        }

        public TripDetailsCom_RS PytonbookdtTOCommonRs(GetBookingDetailsResponse response)
        {
            TripDetailsCom_RS flbookdt = new TripDetailsCom_RS();
            TravelItinerary travelitenary = new TravelItinerary();
            ItineraryInfo iteninfo = new ItineraryInfo();
            List<BusinessEntities.CustomerInfo> custinfo = new List<BusinessEntities.CustomerInfo>();
            ItineraryPricing price = new ItineraryPricing();
            Age agedetails = new Age();
            EquiFare eqfare = new EquiFare();
            Tax tax1 = new Tax();
            TotalFare totalFare = new TotalFare();
            EquiFare eqfaredt = new EquiFare();
            TotalFare totfaredet = new TotalFare();
            List<ReservationItem> reservinfo = new List<ReservationItem>();
            List<TripDetailsPTCFareBreakdown> farebreak = new List<TripDetailsPTCFareBreakdown>();
            //BusinessEntities.CustomerInfo nsd = new BusinessEntities.CustomerInfo();
            //nsd.eTickets = new List<object>();

            //nsd.eTickets.Add("");
               Data data = new Data();
            
            foreach (var k in response.PassengerInfo)
            {
              
                List<SsR> ssrdetails = new List<SsR>();
                Customer cus = new Customer();
                PaxName px = new PaxName();
                px.passengerFirstName = k.FirstName;
                px.passengerLastName = k.LastName;
                px.passengerTitle = "";
                cus.paxName = px;
                agedetails.months = "";
                agedetails.years = "";
                cus.age = agedetails;
                cus.dateOfBirth = Convert.ToDateTime(k.Birthday);
                cus.emailAddress = "";
                cus.gender = k.Sex.ToString();
                cus.knownTravelerNo = "";
                cus.nameNumber = 0;
                cus.nationalID = k.PassportNumber;
                cus.passengerNationality = k.PassportDetails.CountryCode.ToString();
                cus.passportIssuanceCountry = k.PassportDetails.IssueCity;
                cus.passportNationality = k.PassportDetails.CountryCode.ToString();
                cus.passportExpiresOn = Convert.ToDateTime(k.PassportDetails.ExpiryDate);
                cus.passportNumber = k.PassportNumber;
                cus.phoneNumber = "";
                cus.postCode = "";
                cus.redressNo = "";

                ssrdetails.Add(new SsR
                {
                    mealPreference = "",
                    seatPreference = "",
                    itemRPH = 0,

                });
                //custinfo.Add(new BusinessEntities.CustomerInfo()
                //{

                //    customer = cus,
                //    ssRs = ssrdetails,
                //    eTickets=new List<object>();
                //   e

                BusinessEntities.CustomerInfo nsd = new BusinessEntities.CustomerInfo();
                nsd.eTickets = new List<object>();
                nsd.customer = cus;
                nsd.ssRs = ssrdetails;            
                custinfo.Add(nsd);

            }
            float tottax;
            if (response.FlightDetails.Roundtrip == true)
            {
                float tot = response.FlightDetails.TotalFare / 100;
                float miscfeeinbound = response.FlightDetails.Inbound.Taxes / 100;
                float taxinbound = response.FlightDetails.Inbound.Taxes / 100;
                float miscfeeoutbound = response.FlightDetails.Inbound.Taxes / 100;
                float taxoutbound = response.FlightDetails.Inbound.Taxes / 100;
                tottax = miscfeeinbound + miscfeeoutbound + taxinbound + taxoutbound;
                float eqf = tot - (miscfeeinbound + miscfeeoutbound + taxinbound + taxoutbound);
                eqfaredt.amount = eqf.ToString();

            }
            else
            {
                float tot = response.FlightDetails.TotalFare / 100;
                float miscfee = response.FlightDetails.Outbound.MiscFees / 100;
                float tax = response.FlightDetails.Outbound.Taxes / 100;
                tottax = miscfee + tax;
                float eqf = tot - (miscfee + tax);
                eqfaredt.amount = eqf.ToString();
            }
            eqfaredt.currencyCode = response.CurrencyCode.ToString();
            eqfaredt.decimalPlaces = 0;
            price.equiFare = eqfaredt;
            float totfare = response.FlightDetails.TotalFare / 100;
            totfaredet.amount = totfare.ToString();
            totfaredet.currencyCode = response.CurrencyCode.ToString();
            totfaredet.decimalPlaces = 0;
            price.totalFare = totfaredet;
            tax1.amount = tottax.ToString();
            tax1.currencyCode = response.CurrencyCode.ToString();
            tax1.decimalPlaces = 0;
            price.tax = tax1;
            int totalallsegments1 = 0;
            int airsegmentsIndex = response.FlightDetails.Inbound != null
                                ? (response.FlightDetails.Inbound.Legs.Length + response.FlightDetails.Outbound.Legs.Length)
                                : response.FlightDetails.Outbound.Legs.Length;
            if (response.FlightDetails.RealRoundtrip == true)
            {
                totalallsegments1 = 2;
            }
            else
            {
                totalallsegments1 = 1;
            }
            int segments = 0;
            for (int FLseg = 0; FLseg < totalallsegments1; FLseg++)
            {
                if (FLseg == 1)
                {
                    foreach (var k in response.FlightDetails.Inbound.Legs)
                    {
                        reservinfo.Add(new ReservationItem
                        {
                            airEquipmentType = k.CarCode,
                            airlinePNR = response.BookFlightsResult.Inbound.ToString(),
                            arrivalAirportLocationCode = k.DestCode,
                            arrivalDateTime = k.DepDateTime,
                            arrivalTerminal = "",
                            baggage = response.FlightDetails.BaggageOptions[0].Weight.ToString(),
                            cabinClassText = "",
                            departureAirportLocationCode = k.DepCode,
                            departureDateTime = k.ArrDateTime,
                            departureTerminal = "",
                            flightNumber = k.FlightNo,
                            itemRPH = 2,
                            journeyDuration = "",
                            marketingAirlineCode = k.CarCode,
                            numberInParty = 0,
                            operatingAirlineCode = k.CarCode,
                            resBookDesigCode = "",
                            resBookDesigText = "",
                            stopQuantity = 0,
                        });

                    }

                }
                else
                {
                    foreach (var k in response.FlightDetails.Outbound.Legs)
                    {
                        reservinfo.Add(new ReservationItem
                        {
                            airEquipmentType = k.CarCode,
                            airlinePNR = response.BookFlightsResult.Outbound.ToString(),
                            arrivalAirportLocationCode = k.DestCode,
                            arrivalDateTime = k.DepDateTime,
                            arrivalTerminal = "",
                            baggage = response.FlightDetails.BaggageOptions[0].Weight.ToString(),
                            cabinClassText = "",
                            departureAirportLocationCode = k.DepCode,
                            departureDateTime = k.ArrDateTime,
                            departureTerminal = "",
                            flightNumber = k.FlightNo,
                            itemRPH = 1,
                            journeyDuration = "",
                            marketingAirlineCode = k.CarCode,
                            numberInParty = 0,
                            operatingAirlineCode = k.CarCode,
                            resBookDesigCode = "",
                            resBookDesigText = "",
                            stopQuantity = 0,
                        });

                    }

                }
            }
            int adcount =  int.Parse(response.SearchFlightParams.NumADT.ToString());
            int chdcount = int.Parse(response.SearchFlightParams.NumCHD.ToString());
            int infcount = int.Parse(response.SearchFlightParams.NumINF.ToString());
            int totpax = 1;
            if (chdcount > 0)
            {
                if (chdcount > 0)
                {
                    totpax = 3;
                }
                else
                {
                    totpax = 2;
                }
            }
            else
            {
                if (infcount > 0)
                {
                    totpax = 2;
                }

                else
                {
                    totpax = 1;
                }
            }
            float tottax1;
            float totfare1;
            float totalfarep;
            string paxunit;
            int countpas;
            float totpass = float.Parse(response.SearchFlightParams.NumADT.ToString()) + float.Parse(response.SearchFlightParams.NumCHD.ToString()) + float.Parse(response.SearchFlightParams.NumINF.ToString());
            for (int pax = 0; pax < totpax; pax++)
            {
                TripDetailsPassengerFare pasfare = new TripDetailsPassengerFare();
                PassengerTypeQuantity pqt = new PassengerTypeQuantity();
                TotalFare2 fare2 = new TotalFare2();
                Tax2 tax2 = new Tax2();
                EquiFare2 eqdt2 = new EquiFare2();
                if (pax == 0)
                {
                    if (response.FlightDetails.Roundtrip == true)
                    {
                        float adtfareoutbound = response.FlightDetails.Outbound.FareADT / 100 * adcount;
                        float adtfareinbound = response.FlightDetails.Inbound.FareADT / 100 * adcount;
                        totalfarep = adtfareoutbound + adtfareinbound;
                        float miscfeeinbound = response.FlightDetails.Inbound.MiscFees / 100;
                        float taxinbound = response.FlightDetails.Inbound.Taxes / 100;
                        float miscfeeoutbound = response.FlightDetails.Inbound.MiscFees / 100;
                        float taxoutbound = response.FlightDetails.Inbound.Taxes / 100;
                        float tottax2 = miscfeeinbound + miscfeeoutbound + taxinbound + taxoutbound;
                        tottax1 = tottax2 / totpax;
                        totfare1 = totalfarep + tottax1;
                        
                    }

                    else
                    {
                        totalfarep = response.FlightDetails.Outbound.FareADT / 100 * adcount;
                        float miscfee = response.FlightDetails.Outbound.MiscFees / 100;
                        float tax = response.FlightDetails.Outbound.Taxes / 100;
                        float tottax2 = miscfee + tax;
                        tottax1 = tottax2 / totpax;
                        totfare1 = totalfarep + tottax1;

                    }
                    paxunit = "ADT";
                    countpas = adcount;
                }

                else if (pax == 1)
                {
                    int chdnum = int.Parse(response.SearchFlightParams.NumCHD.ToString());
                    if (chdnum == 0)
                    {
                        if (response.FlightDetails.Roundtrip == true)
                        {

                            float inffareoutbound = response.FlightDetails.Outbound.FareINF / 100 * infcount;
                            float inffareinbound = response.FlightDetails.Inbound.FareINF / 100 * infcount;
                            totalfarep = inffareoutbound + inffareinbound;
                            float miscfeeinbound = response.FlightDetails.Inbound.MiscFees / 100;
                            float taxinbound = response.FlightDetails.Inbound.Taxes / 100;
                            float miscfeeoutbound = response.FlightDetails.Inbound.MiscFees / 100;
                            float taxoutbound = response.FlightDetails.Inbound.Taxes / 100;
                            float tottax2 = miscfeeinbound + miscfeeoutbound + taxinbound + taxoutbound;
                            tottax1 = tottax2 / totpax;
                            totfare1 = totalfarep + tottax1;

                        }

                        else
                        {
                             totalfarep = response.FlightDetails.Outbound.FareINF / 100 * infcount;
                            float miscfee = response.FlightDetails.Outbound.Taxes / 100;
                            float tax = response.FlightDetails.Outbound.Taxes / 100;
                            float tottax2 = miscfee + tax;
                            tottax1 = tottax2 / totpax;
                            totfare1 = totalfarep + tottax1;

                        }
                        paxunit = "INF";
                        countpas = infcount;
                    }
                    else

                    {
                        if (response.FlightDetails.Roundtrip == true)
                        {

                            float chdfareoutbound = response.FlightDetails.Outbound.FareCHD / 100 * chdcount;
                            float chdfareinbound = response.FlightDetails.Inbound.FareCHD / 100 * chdcount;
                             totalfarep = chdfareoutbound + chdfareinbound;
                            float miscfeeinbound = response.FlightDetails.Inbound.MiscFees / 100;
                            float taxinbound = response.FlightDetails.Inbound.Taxes / 100;
                            float miscfeeoutbound = response.FlightDetails.Inbound.MiscFees / 100;
                            float taxoutbound = response.FlightDetails.Inbound.Taxes / 100;
                            float tottax2 = miscfeeinbound + miscfeeoutbound + taxinbound + taxoutbound;
                            tottax1 = tottax2 / totpax;
                            totfare1 = totalfarep + tottax1;

                        }

                        else
                        {
                            totalfarep = response.FlightDetails.Outbound.FareCHD / 100 * chdcount;
                            float miscfee = response.FlightDetails.Outbound.MiscFees / 100;
                            float tax = response.FlightDetails.Outbound.Taxes / 100;
                            float tottax2 = miscfee + tax;
                            tottax1 = tottax2 / totpax;
                            totfare1 = totalfarep + tottax1;

                        }

                    }
                    paxunit = "CHD";
                    countpas = chdcount;
                }

                else
                {
                    if (response.FlightDetails.Roundtrip == true)
                    {

                        float chdfareoutbound = response.FlightDetails.Outbound.FareINF / 100 * infcount;
                        float chdfareinbound = response.FlightDetails.Inbound.FareINF / 100 * infcount;
                        totalfarep = chdfareoutbound + chdfareinbound;
                        float miscfeeinbound = response.FlightDetails.Inbound.MiscFees / 100;
                        float taxinbound = response.FlightDetails.Inbound.Taxes / 100;
                        float miscfeeoutbound = response.FlightDetails.Inbound.MiscFees / 100;
                        float taxoutbound = response.FlightDetails.Inbound.Taxes / 100;
                        float tottax2 = miscfeeinbound + miscfeeoutbound + taxinbound + taxoutbound;
                        tottax1 = tottax2 / totpax;
                        totfare1 = totalfarep + tottax1;

                    }
                    else
                    {
                        totalfarep = response.FlightDetails.Outbound.FareINF / 100 * infcount;
                        float miscfee = response.FlightDetails.Outbound.MiscFees / 100;
                        float tax = response.FlightDetails.Outbound.Taxes / 100;
                        float tottax2 = miscfee + tax;
                        tottax1 = tottax2 / totpax;
                        totfare1 = totalfarep + tottax1;

                    }

                    paxunit = "INF";
                    countpas = infcount;
                }

                fare2.amount = totfare1.ToString();
                fare2.currencyCode = response.SearchFlightParams.CurrencyCode.ToString();
                fare2.decimalPlaces = 0;
                tax2.amount = tottax1.ToString();
                tax2.currencyCode = response.SearchFlightParams.CurrencyCode.ToString();
                tax2.decimalPlaces = 0;
                eqdt2.amount = totalfarep.ToString();
                eqdt2.currencyCode = response.SearchFlightParams.CurrencyCode.ToString();
                eqdt2.decimalPlaces = 0;
                pasfare.totalFare = fare2;
                pasfare.tax = tax2;
                pasfare.equiFare = eqdt2;
                pqt.code = 1;
                pqt.quantity = countpas;
                farebreak.Add(new TripDetailsPTCFareBreakdown
                {
                    passengerTypeQuantity=pqt,
                    tripDetailsPassengerFare= pasfare,

                });

            }
            iteninfo.customerInfos = custinfo;
            iteninfo.reservationItems = reservinfo;
            iteninfo.tripDetailsPTC_FareBreakdowns = farebreak;
            iteninfo.itineraryPricing = price;
            iteninfo.clientUTCOffset = 0;
            iteninfo.bookedReservedItems= new List<object>();
            if (response.BookFlightsResult.Outbound.BookingStatus.ToString() == "Confirmed")
            {
                travelitenary.bookingStatus = "OK";
            }

           else
            {
                travelitenary.bookingStatus = "HK";
            }
            //travelitenary.bookingStatus = response.BookFlightsResult.Outbound.BookingStatus.ToString();
            travelitenary.itineraryInfo = iteninfo;
            travelitenary.uniqueID = response.BookFlightsResult.Outbound.BookingCode;
            if (response.BookFlightsResult.Outbound.BookingStatus.ToString() == "Confirmed")
            {
                travelitenary.ticketStatus = "OK";
            }

            else
            {
                travelitenary.ticketStatus = "HK";
            }
            travelitenary.bookingNotes = new List<object>();
            flbookdt.success = true;
            flbookdt.travelItinerary = travelitenary;
            flbookdt.errors = new List<object>();                  
            return flbookdt;
        }

        public FLbookRS PytonpreparebookTOCommonRs(PrepareBookFlightsResponse response, string pq)
        {
            FLbookRS flbook = new FLbookRS();

            if (response.Booked == true)
            {
                BusinessEntities.Bookflightresult book = new BusinessEntities.Bookflightresult()
                {
                    Status = "NOT Confirmed",
                    Success = "true",
                    Target = pq,
                    TktTimeLimit = "NA",
                    UniqueID = "NIL",
                    Airlinepnr = "NIL",
                    Sessionid = "NA",
                    Faresourcecode = "NA",
                    AgencyCode = "Albadie"

                };

                Error err = new Error()
                {
                    Code = "0",
                    Message = "NIL"

                };

                Error[] errors = new Error[] { err };

                book.Errors = errors;

                flbook.BookFlightResult = book;

            }

            else
            {
                BusinessEntities.Bookflightresult book = new BusinessEntities.Bookflightresult()
                {

                    Status = "pending",
                    Success = "false",
                    Target = pq,
                    TktTimeLimit = "NA",
                    UniqueID = "NIL",
                    Airlinepnr = "NIL",
                    Sessionid = "NA",
                    Faresourcecode = "NA",
                    AgencyCode = "NA"

                };

                Error err = new Error()
                {
                    Code = "0",
                    Message = "NIL"

                };

                Error[] errors = new Error[] { err };

                book.Errors = errors;

                flbook.BookFlightResult = book;

            }

            return flbook;

        }
    }
}


