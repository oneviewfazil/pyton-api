﻿namespace PytonPartner.Enumeration
{
    public enum PaymentCode
    {

        /// <remarks/>
        AMEX,

        /// <remarks/>
        VISA,

        /// <remarks/>
        MASTERCARD,

        /// <remarks/>
        MASTERPREPAID,

        /// <remarks/>
        ELECTRON,

        /// <remarks/>
        ELV,

        /// <remarks/>
        ELSYARRES,

        /// <remarks/>
        DINERS,

        /// <remarks/>
        AIRPLUS,

        /// <remarks/>
        UATP,

        /// <remarks/>
        CARTEBLEU,

        /// <remarks/>
        JCB,

        /// <remarks/>
        IDEAL,

        /// <remarks/>
        MAESTRO,

        /// <remarks/>
        DIRECTEBANKING,

        /// <remarks/>
        MASTERDEBIT,

        /// <remarks/>
        VISADEBIT,

        /// <remarks/>
        CBC,

        /// <remarks/>
        KBC,
    }
}
