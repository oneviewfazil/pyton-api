﻿namespace PytonPartner
{

    using BusinessServices;
    using BusinessServices.Interface;
    using Resolver;
    using System.ComponentModel.Composition;

    [Export(typeof(IComponent))]
    public class DependencyResolver : IComponent
    {
        public void SetUp(IRegisterComponent registerComponent)
        {
            registerComponent.RegisterType<IPytonServices, PytonServices>();
            registerComponent.RegisterType<IPytonRequest, PytonRequest>();

        }
    }
}
