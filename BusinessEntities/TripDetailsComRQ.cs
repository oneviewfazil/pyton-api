﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessEntities
{
    public class TripDetailscomRQ
    {
        public string UniqueID { get; set; }
        public string Target { get; set; }
        public string AccountNumber { get; set; }
        public string Password { get; set; }
        public string UserName { get; set; }

        public Supplieragencydetail[] SupplierAgencyDetails { get; set; }

    }
}
