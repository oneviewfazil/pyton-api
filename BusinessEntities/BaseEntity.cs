﻿namespace BusinessEntities
{
    using System;
    public class BaseEntity
    {
        public int? CreatedUser { get; set; }
        public DateTime? CreatedDate { get; set; }
        public int? ModifiedUser { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string StatusCode { get; set; }
        public string StatusMessage { get; set; }
    }
}
