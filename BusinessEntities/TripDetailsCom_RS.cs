﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessEntities
{
   public class TripDetailsCom_RS
    {
        public List<object> errors { get; set; }
        public bool success { get; set; }
        public int target { get; set; }
        public TravelItinerary travelItinerary { get; set; }
    }
    public class Age
    {
        public string months { get; set; }
        public string years { get; set; }
    }

    public class PaxName
    {
        public string passengerFirstName { get; set; }
        public string passengerLastName { get; set; }
        public string passengerTitle { get; set; }
    }

    public class Customer
    {
        public Age age { get; set; }
        public DateTime dateOfBirth { get; set; }
        public string emailAddress { get; set; }
        public string gender { get; set; }
        public string knownTravelerNo { get; set; }
        public int nameNumber { get; set; }
        public string nationalID { get; set; }
        public string passengerNationality { get; set; }
        public int passengerType { get; set; }
        public DateTime passportExpiresOn { get; set; }
        public string passportIssuanceCountry { get; set; }
        public string passportNationality { get; set; }
        public string passportNumber { get; set; }
        public PaxName paxName { get; set; }
        public string phoneNumber { get; set; }
        public string postCode { get; set; }
        public string redressNo { get; set; }
    }

    public class SsR
    {
        public int itemRPH { get; set; }
        public string mealPreference { get; set; }
        public string seatPreference { get; set; }
    }

    public class CustomerInfo
    {
        public Customer customer { get; set; }
        public List<object> eTickets { get; set; }
        public List<SsR> ssRs { get; set; }

        public static implicit operator CustomerInfo(Customerinfo v)
        {
            throw new NotImplementedException();
        }
    }

    public class EquiFare
    {
        public string amount { get; set; }
        public string currencyCode { get; set; }
        public int decimalPlaces { get; set; }
    }

    public class Tax
    {
        public string amount { get; set; }
        public string currencyCode { get; set; }
        public int decimalPlaces { get; set; }
    }

    public class TotalFare
    {
        public string amount { get; set; }
        public string currencyCode { get; set; }
        public int decimalPlaces { get; set; }
    }

    public class ItineraryPricing
    {
        public EquiFare equiFare { get; set; }
        public Tax tax { get; set; }
        public TotalFare totalFare { get; set; }
    }

    public class ReservationItem
    {
        public string airEquipmentType { get; set; }
        public string airlinePNR { get; set; }
        public string arrivalAirportLocationCode { get; set; }
        public DateTime arrivalDateTime { get; set; }
        public string arrivalTerminal { get; set; }
        public string baggage { get; set; }
        public string cabinClassText { get; set; }
        public string departureAirportLocationCode { get; set; }
        public DateTime departureDateTime { get; set; }
        public string departureTerminal { get; set; }
        public string flightNumber { get; set; }
        public int itemRPH { get; set; }
        public string journeyDuration { get; set; }
        public string marketingAirlineCode { get; set; }
        public int numberInParty { get; set; }
        public string operatingAirlineCode { get; set; }
        public string resBookDesigCode { get; set; }
        public string resBookDesigText { get; set; }
        public int stopQuantity { get; set; }
    }

    public class PassengerTypeQuantity
    {
        public int code { get; set; }
        public int quantity { get; set; }
    }

    public class EquiFare2
    {
        public string amount { get; set; }
        public string currencyCode { get; set; }
        public int decimalPlaces { get; set; }
    }

    public class Tax2
    {
        public string amount { get; set; }
        public string currencyCode { get; set; }
        public int decimalPlaces { get; set; }
    }

    public class TotalFare2
    {
        public string amount { get; set; }
        public string currencyCode { get; set; }
        public int decimalPlaces { get; set; }
    }

    public class TripDetailsPassengerFare
    {
        public EquiFare2 equiFare { get; set; }
        public Tax2 tax { get; set; }
        public TotalFare2 totalFare { get; set; }
    }

    public class TripDetailsPTCFareBreakdown
    {
        public PassengerTypeQuantity passengerTypeQuantity { get; set; }
        public TripDetailsPassengerFare tripDetailsPassengerFare { get; set; }
    }

    public class ItineraryInfo
    {
        public List<object> bookedReservedItems { get; set; }
        public int clientUTCOffset { get; set; }
        public List<CustomerInfo> customerInfos { get; set; }
        public ItineraryPricing itineraryPricing { get; set; }
        public List<ReservationItem> reservationItems { get; set; }
        public List<TripDetailsPTCFareBreakdown> tripDetailsPTC_FareBreakdowns { get; set; }
    }

    public class TravelItinerary
    {
        public List<object> bookingNotes { get; set; }
        public string bookingStatus { get; set; }
        public ItineraryInfo itineraryInfo { get; set; }
        public string ticketStatus { get; set; }
        public string uniqueID { get; set; }
    }

    public class Data
    {
        public List<object> errors { get; set; }
       
    }

  
}
