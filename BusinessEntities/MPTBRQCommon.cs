﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessEntities
{

    public class Rootobject
    {
        public Commonrequestsearch CommonRequestSearch { get; set; }
        public Supplieragencydetail[] SupplierAgencyDetails { get; set; }
        public Origindestinationinformation[] OriginDestinationInformation { get; set; }
        public string Currency { get; set; }
        public string PreferredAirline { get; set; }
        public string NonStop { get; set; }
        public string cabin { get; set; }
        public string IsRefundable { get; set; }
        public string Maxstopquantity { get; set; }
        public string Triptype { get; set; }
        public string PreferenceLevel { get; set; }
        public string Target { get; set; }
        public Passengertypequantity PassengerTypeQuantity { get; set; }
    }

    public class Commonrequestsearch
    {
        public string AgencyCode { get; set; }
        public int numberOfUnits { get; set; }
        public string typeOfUnit { get; set; }
    }

    public class Passengertypequantity
    {
        public int ADT { get; set; }
        public int CHD { get; set; }
        public int INF { get; set; }
    }

   

    public class Origindestinationinformation
    {
        public string DepartureDate { get; set; }
        public string OriginLocation { get; set; }
        public string DestinationLocation { get; set; }
        public Radiusinformation RadiusInformation { get; set; }
    }

    
    public class Radiusinformation
    {
        public string _FromValue { get; set; }
        public string _ToValue { get; set; }
    }
}
