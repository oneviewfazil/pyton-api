﻿namespace BusinessEntities.Common
{
    using System;
    using System.IO;
    using System.Web;
    using System.Net.Http;
    using System.Web.Http;

    public static class CustomExtention
    {
        public static DateTime GetDateTimeFromString(this string dateTime)
        {
            string Year = ""; string Month = ""; string Day = "";
            Year = dateTime.Split('-')[2];
            Month = dateTime.Split('-')[1];
            Day = dateTime.Split('-')[0];
            return Convert.ToDateTime(Year + "-" + Month + "-" + Day);
        }
        public static string GetDayMonthYear(DateTime dateTime)
        {
            string dateTimeStr = dateTime.ToString("dd-MM-yy-HH-mm-ss");
            string AirlegDepYear = dateTimeStr.Split('-')[2];
            string AirlegDepMonth = dateTimeStr.Split('-')[1];
            string AirlegDepDay = dateTimeStr.Split('-')[0];
            return AirlegDepDay + AirlegDepMonth + AirlegDepYear;
        }
        public static string GetHourMinute(DateTime dateTime)
        {
            string dateTimeStr = dateTime.ToString("dd-MM-yy-HH-mm-ss");
            string AirlegDepHour = dateTimeStr.Split('-')[3];
            string AirlegDepMinute = dateTimeStr.Split('-')[4];
            return AirlegDepHour + AirlegDepMinute;
        }
        public static string GenerateFlightKey(string flightFromKey, string flightToKey, DateTime deptDate, string operatingAirlineCode, string flightNumber)
        {
            string FlightDepdateKey = deptDate.ToString("dd-MM-yy-HH-mm-ss");
            string YearKey = FlightDepdateKey.Split('-')[2];
            string MonthKey = FlightDepdateKey.Split('-')[1];
            string DayKey = FlightDepdateKey.Split('-')[0];
            string HourKey = FlightDepdateKey.Split('-')[3];
            string MinKey = FlightDepdateKey.Split('-')[4];
            return flightFromKey + "-" + flightToKey + "-" + DayKey + MonthKey + YearKey + "-" + HourKey + MinKey + "-" + operatingAirlineCode + "-" + flightNumber;
        }
        public static void SaveLog(string logName, string agency, string supplier, string rQ, string rS)
        {
            //try
            //{
            //    string date = DateTime.Now.ToString("dd-MM-yyyy");
            //    //string storePath = @"C:\Users\SHABEER\Documents\Source\Repos\mystiflynewapi\PartnerOne" + "/LOGRQRS/" + date + "/" + agency + "/" + supplier + "/";
            //    string storePath = @"C:\Inetpub\vhosts\oneviewitsolutions.com\API\logs\pyton\LOGRQRS\" + agency + "/" + date + "/" + supplier + "/";

            //    if (!Directory.Exists(storePath))
            //        Directory.CreateDirectory(storePath);
            //    string timeStamp = DateTime.Now.ToString("dd-MM-yyyy-HH-mm-ss-ffff");
            //    string filenameRq = storePath + "/" + logName + "_" + timeStamp + "_RQ.json";
            //    File.WriteAllText(filenameRq, rQ);
            //    string filenameRs = storePath + "/" + logName + "_" + timeStamp + "_RS.json";
            //    File.WriteAllText(filenameRs, rS);
            //}
            //catch (Exception)
            //{

            //}

         
                try
                {
                    string date = DateTime.Now.ToString("dd-MM-yyyy");
                    //string storePath = @"C:\Users\SHABEER\Documents\Source\Repos\mystiflynewapi\PartnerOne" + "/LOGRQRS/" +agency  + "/" + date + "/" + supplier + "/" + logName + "/";
                    string storePath = @"C:\Inetpub\vhosts\oneviewitsolutions.com\API\logs\pyton\LOGRQRS\" + agency + "/" + date + "/" + supplier + "/" + logName + "/";

                    if (!Directory.Exists(storePath))
                        Directory.CreateDirectory(storePath);
                    string timeStamp = DateTime.Now.ToString("dd-MM-yyyy-HH-mm-ss-ffff");
                    string filenameRq = storePath + "/" + logName + "_" + timeStamp + "_RQ.json";
                    File.WriteAllText(filenameRq, rQ);
                    string filenameRs = storePath + "/" + logName + "_" + timeStamp + "_RS.json";
                    File.WriteAllText(filenameRs, rS);
                }
                catch (Exception)
                {

                }
            }
    }
}
