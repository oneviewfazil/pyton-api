﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessEntities
{
    public class Supplieragencydetail
    {
        public int SupplierId { get; set; }
        public string BaseUrl { get; set; }
        public object RequestUrl { get; set; }
        public int AgencyID { get; set; }
        public string AgencyCode { get; set; }
        public string SupplierName { get; set; }
        public string SupplierCode { get; set; }
        public string AccountNumber { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Status { get; set; }
        public string ToCurrency { get; set; }
        public float ToROEValue { get; set; }
    }
}
