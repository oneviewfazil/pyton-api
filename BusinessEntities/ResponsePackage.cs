﻿using System;
namespace BusinessEntities
{
    using System.Collections.Generic;
    using System.Net.Http;

    public class ResponsePackage
    {
        public List<string> Errors { get; set; }

        public object Data { get; set; }
        //public bool IsSuccessful { get; set; }
        //public string Message { get; set; }
        //public HttpResponseMessage ResponseMessage { get; set; }
        public ResponsePackage(object result, List<string> errors)
        {
            Errors = errors;
            Data = result;
        }
        //public ResponsePackage(object result, List<string> errors, bool isSuccessful, HttpResponseMessage responseMessage)
        //{
        //    Errors = errors;
        //    Data = result;
        //    IsSuccessful = isSuccessful;
        //    ResponseMessage = responseMessage;
        //}
    }
}
