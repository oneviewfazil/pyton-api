﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessEntities.Entity;

namespace BAL.BusinessServices.Interface
{
    public interface IPartnerServices
    {
        Task<IEnumerable<PartnerEntity>> GetPartners();
        IEnumerable<PartnerEntity> GetPartnersSync();
    }
}
