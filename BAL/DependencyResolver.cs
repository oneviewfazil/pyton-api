﻿namespace BAL
{
    using System.ComponentModel.Composition;
    using Resolver;
    using BAL.BusinessServices;
    using BAL.BusinessServices.Interface;
    using DAL.Core.Repositories;

    [Export(typeof(IComponent))]
    public class DependencyResolver : IComponent
    {
        public void SetUp(IRegisterComponent registerComponent)
        {
            registerComponent.RegisterType<IPartnerServices, PartnerServices>();
          //  registerComponent.RegisterType<IRepositoryContainer, RepositoryContainer>();

        }
    }
}
