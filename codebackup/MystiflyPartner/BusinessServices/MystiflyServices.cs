﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessEntities.Entity;
using MystiflyPartner.BusinessServices.Interface;
using System.Linq;

namespace MystiflyPartner.BusinessServices
{
    public class MystiflyServices : IMystiflyServices
    {
        string ServicePath = "http://onepointdemo.myfarebox.com/V2/OnePoint.svc";

        public string CreateSession(string input)
        {
            string soapAction = "Mystifly.OnePoint/OnePoint/CreateSession";
            StringBuilder sb = new StringBuilder();

            sb.Append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:mys=\"Mystifly.OnePoint\" xmlns:mys1=\"http://schemas.datacontract.org/2004/07/Mystifly.OnePoint\">");
            sb.Append("   <soapenv:Header/>");
            sb.Append("   <soapenv:Body>");
            sb.Append("     <mys:CreateSession>");

            sb.Append("         <mys:rq>");

            sb.Append("            <mys1:AccountNumber>MCN001438</mys1:AccountNumber>");

            sb.Append("            <mys1:Password>ONEV2017_xml</mys1:Password>");

            sb.Append("           ");

            sb.Append("            <mys1:UserName>ONEV_XML</mys1:UserName>");
            sb.Append("         </mys:rq>");
            sb.Append("      </mys:CreateSession>");
            sb.Append("   </soapenv:Body>");
            sb.Append("</soapenv:Envelope>");
            string query = sb.ToString();


            string sessionresponse = SOAPHelper.SendSOAPRequest(query, ServicePath, "POST", soapAction);



            return sessionresponse;

        }

        public string Search(string SessionID)
        {
            string soapAction = "Mystifly.OnePoint/OnePoint/AirLowFareSearch";

            string from = "LHR";
            string To = "NYC";
            string Dtime = "2017-12-05T00:00:00";



            StringBuilder sb = new StringBuilder();

            sb.Append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:mys=\"Mystifly.OnePoint\" xmlns:mys1=\"http://schemas.datacontract.org/2004/07/Mystifly.OnePoint\" xmlns:arr=\"http://schemas.microsoft.com/2003/10/Serialization/Arrays\">");
            sb.Append("   <soapenv:Header/>");
            sb.Append("   <soapenv:Body>");
            sb.Append("     <mys:AirLowFareSearch>");
            sb.Append("         <mys:rq>");
            sb.Append("            <mys1:IsRefundable>false</mys1:IsRefundable>");
            sb.Append("            <mys1:IsResidentFare>false</mys1:IsResidentFare>");
            sb.Append("            <mys1:NearByAirports>false</mys1:NearByAirports>");
            sb.Append("            <mys1:OriginDestinationInformations>");
            sb.Append("               <mys1:OriginDestinationInformation>");
            sb.Append("                  <mys1:DepartureDateTime>" + Dtime + "</mys1:DepartureDateTime>");
            sb.Append("                  <mys1:DestinationLocationCode>" + To + "</mys1:DestinationLocationCode>");
            sb.Append("                  <mys1:OriginLocationCode>" + from + "</mys1:OriginLocationCode>");
            sb.Append("               </mys1:OriginDestinationInformation>");
            sb.Append("               ");
            sb.Append("              ");
            sb.Append("              ");
            sb.Append("            </mys1:OriginDestinationInformations>");
            sb.Append("            <mys1:PassengerTypeQuantities>");
            sb.Append("               <mys1:PassengerTypeQuantity>");
            sb.Append("                  <mys1:Code>ADT</mys1:Code>");
            sb.Append("                  <mys1:Quantity>1</mys1:Quantity>");
            sb.Append("               </mys1:PassengerTypeQuantity>");
            sb.Append("            </mys1:PassengerTypeQuantities>");
            sb.Append("            <mys1:PricingSourceType>All</mys1:PricingSourceType>");
            sb.Append("            <mys1:RequestOptions>Fifty</mys1:RequestOptions>");
            sb.Append("            <mys1:SessionId>" + SessionID + "</mys1:SessionId>");
            sb.Append("            <mys1:Target>Production</mys1:Target>");
            sb.Append("            <mys1:TravelPreferences>");
            sb.Append("               <mys1:AirTripType>OneWay</mys1:AirTripType>");
            sb.Append("               <mys1:MaxStopsQuantity>All</mys1:MaxStopsQuantity>");
            sb.Append("               <mys1:Preferences>");
            sb.Append("                  <mys1:CabinClassPreference>");
            sb.Append("                     <mys1:CabinType>Y</mys1:CabinType>");
            sb.Append("                     <mys1:PreferenceLevel>Preferred</mys1:PreferenceLevel>");
            sb.Append("                  </mys1:CabinClassPreference>");
            sb.Append("               </mys1:Preferences>");
            sb.Append("             ");
            sb.Append("          ");
            sb.Append("            </mys1:TravelPreferences>");
            sb.Append("         </mys:rq>");
            sb.Append("      </mys:AirLowFareSearch>");
            sb.Append("   </soapenv:Body>");
            sb.Append("</soapenv:Envelope>");



            string query = sb.ToString();


            string response = SOAPHelper.SendSOAPRequest(query, ServicePath, "POST", soapAction);
            return response;

        }

        public string Requestfarerule(string SessionID, string from, string to, string farebasiscode, string validatingcarrier, string Mcarrier, string Opcarier, string dettime)
        {
            string soapAction = "Mystifly.OnePoint/OnePoint/FareRules";


            StringBuilder sb = new StringBuilder();

            sb.Append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:mys=\"Mystifly.OnePoint\" xmlns:mys1=\"http://schemas.datacontract.org/2004/07/Mystifly.OnePoint\">");
            sb.Append("   <soapenv:Header/>");
            sb.Append("   <soapenv:Body>");
            sb.Append("      <mys:FareRules>");
            sb.Append("         <!--Optional:-->");
            sb.Append("         <mys:rq>");
            sb.Append("            <!--Optional:-->");
            sb.Append("            <mys1:FareInfos>");
            sb.Append("               <!--Zero or more repetitions:-->");
            sb.Append("               <mys1:AirRulesFareInfo>");
            sb.Append("                  <!--Optional:-->");
            sb.Append("                  <mys1:ArrivalAirportLocationCode>COK</mys1:ArrivalAirportLocationCode>");
            sb.Append("                  <!--Optional:-->");
            sb.Append("                  <mys1:DepartureAirportLocationCode>DXB</mys1:DepartureAirportLocationCode>");
            sb.Append("                  <!--Optional:-->");
            sb.Append("                  <mys1:DepartureDateTime>" + dettime + "</mys1:DepartureDateTime>");
            sb.Append("                 ");
            sb.Append("                  <mys1:MarketingAirlineCode>" + Mcarrier + "</mys1:MarketingAirlineCode>");
            sb.Append("                  <!--Optional:-->");
            sb.Append("                  <mys1:OperatingAirlineCode>" + Opcarier + "</mys1:OperatingAirlineCode>");
            sb.Append("               </mys1:AirRulesFareInfo>");
            sb.Append("            </mys1:FareInfos>");
            sb.Append("            <!--Optional:-->");
            sb.Append("            <mys1:FareSourceCode>" + farebasiscode + "</mys1:FareSourceCode>");
            sb.Append("            <!--Optional:-->");
            sb.Append("            <mys1:SessionId>" + SessionID + "</mys1:SessionId>");
            sb.Append("            <!--Optional:-->");
            sb.Append("            <mys1:Target>Production</mys1:Target>");
            sb.Append("            <!--Optional:-->");
            sb.Append("            <mys1:ValidatingAirlineCode>" + validatingcarrier + "</mys1:ValidatingAirlineCode>");
            sb.Append("         </mys:rq>");
            sb.Append("      </mys:FareRules>");
            sb.Append("   </soapenv:Body>");
            sb.Append("</soapenv:Envelope>");



            string query = sb.ToString();


            string response = SOAPHelper.SendSOAPRequest(query, ServicePath, "POST", soapAction);
            return response;

        }

        public string Airrevalidator(string SessionID, string farebasiscode)
        {
            string soapAction = "Mystifly.OnePoint/OnePoint/AirRevalidate";


            StringBuilder sb = new StringBuilder();

            sb.Append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:mys=\"Mystifly.OnePoint\" xmlns:mys1=\"http://schemas.datacontract.org/2004/07/Mystifly.OnePoint\">");
            sb.Append("   <soapenv:Header/>");
            sb.Append("   <soapenv:Body>");
            sb.Append("      <mys:AirRevalidate>");
            sb.Append("         <!--Optional:-->");
            sb.Append("         <mys:rq>");
            sb.Append("            <!--Optional:-->");
            sb.Append("            <mys1:FareSourceCode>" + farebasiscode + "</mys1:FareSourceCode>");
            sb.Append("            <!--Optional:-->");
            sb.Append("            <mys1:SessionId>" + SessionID + "</mys1:SessionId>");
            sb.Append("            <!--Optional:-->");
            sb.Append("            <mys1:Target>Production</mys1:Target>");
            sb.Append("         </mys:rq>");
            sb.Append("      </mys:AirRevalidate>");
            sb.Append("   </soapenv:Body>");
            sb.Append("</soapenv:Envelope>");



            string query = sb.ToString();


            string response = SOAPHelper.SendSOAPRequest(query, ServicePath, "POST", soapAction);
            return response;

        }

        public string Bookflight(string SessionID, string farebasiscode, string requestdetails)
        {
            string soapAction = "Mystifly.OnePoint/OnePoint/BookFlight";




            // BuildMyString.com generated code. Please enjoy your string responsibly.

            StringBuilder sb2 = new StringBuilder();

            sb2.Append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:mys=\"Mystifly.OnePoint\" xmlns:mys1=\"http://schemas.datacontract.org/2004/07/Mystifly.OnePoint\" xmlns:mys2=\"Mystifly.OnePoint.OnePointEntities\">");
            sb2.Append("   <soapenv:Header/>");
            sb2.Append("   <soapenv:Body>");
            sb2.Append("      <mys:BookFlight>");
            sb2.Append("     <mys:rq>");
            sb2.Append("            <mys1:FareSourceCode>" + farebasiscode + "</mys1:FareSourceCode>");
            sb2.Append("            <mys1:SessionId>" + SessionID + "</mys1:SessionId>");
            sb2.Append("        <mys1:Target>Test</mys1:Target>");
            sb2.Append("        <mys1:TravelerInfo>");
            sb2.Append("               <mys1:AirTravelers>");
            sb2.Append("                  <mys1:AirTraveler>");
            sb2.Append("                     <mys1:DateOfBirth>1990-04-01T00:00:00</mys1:DateOfBirth>");
            sb2.Append("                     <mys1:ExtraServices1_1>");
            sb2.Append("                       ");
            sb2.Append("                     ");
            sb2.Append("                         </mys1:ExtraServices1_1>");
            sb2.Append("                       <mys1:Gender>M</mys1:Gender>");
            sb2.Append("                     <mys1:PassengerName>");
            sb2.Append("                        <mys1:PassengerFirstName>fazil</mys1:PassengerFirstName>");
            sb2.Append("                        <mys1:PassengerLastName>Kunnath</mys1:PassengerLastName>");
            sb2.Append("                        <mys1:PassengerTitle>MR</mys1:PassengerTitle>");
            sb2.Append("                     </mys1:PassengerName>");
            sb2.Append("                     <mys1:PassengerType>ADT</mys1:PassengerType>");
            sb2.Append("                     <mys1:Passport>");
            sb2.Append("                        <mys1:Country>UK</mys1:Country>");
            sb2.Append("                        <mys1:ExpiryDate>2023-06-04T00:00:00</mys1:ExpiryDate>");
            sb2.Append("                        <mys1:PassportNumber>IN3456789</mys1:PassportNumber>");
            sb2.Append("                     </mys1:Passport>");
            sb2.Append("                  </mys1:AirTraveler>");
            sb2.Append("                ");
            sb2.Append("                 ");
            sb2.Append("               ");
            sb2.Append("               </mys1:AirTravelers>");
            sb2.Append("          <mys1:AreaCode>141</mys1:AreaCode>");
            sb2.Append("         <mys1:CountryCode>44</mys1:CountryCode>");
            sb2.Append("         <mys1:Email>peter@gmail.com</mys1:Email>");
            sb2.Append("       <mys1:PhoneNumber>5467890</mys1:PhoneNumber>");
            sb2.Append("      <mys1:PostCode>G1 1QN</mys1:PostCode>");
            sb2.Append("            </mys1:TravelerInfo>");
            sb2.Append("         </mys:rq>");
            sb2.Append("      </mys:BookFlight>");
            sb2.Append("   </soapenv:Body>");
            sb2.Append("</soapenv:Envelope>");


            string query = sb2.ToString();


            string response = SOAPHelper.SendSOAPRequest(query, ServicePath, "POST", soapAction);
            return response;

        }

        public string Tripdetails(string SessionID, string uniqueno)
        {
            string soapAction = "Mystifly.OnePoint/OnePoint/TripDetails";


            StringBuilder sb = new StringBuilder();

            sb.Append("<?xml version=\"1.0\"?>");
            sb.Append("<soapenv:Envelope xmlns:mys1=\"http://schemas.datacontract.org/2004/07/Mystifly.OnePoint\" xmlns:mys=\"Mystifly.OnePoint\" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\">");
            sb.Append("<soapenv:Header/>");
            sb.Append("<soapenv:Body>");
            sb.Append("<mys:TripDetails>");
            sb.Append("<mys:rq>");
            sb.Append("<mys1:SendOnlyTicketed>false</mys1:SendOnlyTicketed>");
            sb.Append("<mys1:SessionId>" + SessionID + "</mys1:SessionId>");
            sb.Append("<mys1:Target>Production</mys1:Target>");
            sb.Append("<mys1:UniqueID>" + uniqueno + "</mys1:UniqueID>");
            sb.Append("</mys:rq>");
            sb.Append("</mys:TripDetails>");
            sb.Append("</soapenv:Body>");
            sb.Append("</soapenv:Envelope>");



            string query = sb.ToString();


            string response = SOAPHelper.SendSOAPRequest(query, ServicePath, "POST", soapAction);
            return response;

        }

        public string Ticketorder(string SessionID, string uniqueno)
        {
            string soapAction = "Mystifly.OnePoint/OnePoint/TicketOrder";


            StringBuilder sb = new StringBuilder();

            sb.Append("<?xml version=\"1.0\"?>");
            sb.Append("<soapenv:Envelope xmlns:mys1=\"http://schemas.datacontract.org/2004/07/Mystifly.OnePoint\" xmlns:mys=\"Mystifly.OnePoint\" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\">");
            sb.Append("<soapenv:Header/>");
            sb.Append("<soapenv:Body>");
            sb.Append("<mys:TicketOrder>");
            sb.Append("<mys:rq>");
            sb.Append("<mys1:SessionId>" + SessionID + "</mys1:SessionId>");
            sb.Append("<mys1:Target>Production</mys1:Target>");
            sb.Append("<mys1:UniqueID>" + uniqueno + "</mys1:UniqueID>");
            sb.Append("</mys:rq>");
            sb.Append("</mys:TicketOrder>");
            sb.Append("</soapenv:Body>");
            sb.Append("</soapenv:Envelope>");



            string query = sb.ToString();


            string response = SOAPHelper.SendSOAPRequest(query, ServicePath, "POST", soapAction);
            return response;

        }

        public async Task<IEnumerable<UserEntity>> GetDetails(UserEntity message)
        {
            List<UserEntity> list = new List<UserEntity>();
            UserEntity userEntity = new UserEntity()
            {
                Name = "Fardeen Ahmad",
                Password = message.Password,
                UserId = message.UserId,
                UserName = message.UserName
            };
            list.Add(userEntity);
            list.Add(userEntity);
            list.Add(userEntity);
            return  list.ToList();
        }
    }
}
