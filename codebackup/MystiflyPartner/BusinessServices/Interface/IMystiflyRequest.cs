﻿using BusinessEntities;
using MystiflyPartner.MistiFlyServiceReference;
using System.Threading.Tasks;

namespace MystiflyPartner.BusinessServices
{
    public interface IMystiflyRequest
    {
        SessionCreateRS Getsession(string accountNo, string password, string username);
        Task<AirLowFareSearchRS> FlightSearch(Rootobject message);
        AirLowFareSearchRS FlightSearchSync(Rootobject message);

    }
}