﻿using BusinessEntities.Entity;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MystiflyPartner.BusinessServices.Interface
{
    public interface IMystiflyServices
    {
        string Airrevalidator(string SessionID, string farebasiscode);
        string Bookflight(string SessionID, string farebasiscode, string requestdetails);
        string CreateSession(string input);
        string Requestfarerule(string SessionID, string from, string to, string farebasiscode, string validatingcarrier, string Mcarrier, string Opcarier, string dettime);
        string Search(string SessionID);
        string Ticketorder(string SessionID, string uniqueno);
        string Tripdetails(string SessionID, string uniqueno);
        Task<IEnumerable<UserEntity>> GetDetails(UserEntity message);
    }
}