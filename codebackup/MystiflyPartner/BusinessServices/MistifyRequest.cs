﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MystiflyPartner.MistiFlyServiceReference;
using MystiflyPartner.BusinessServices.Interface;
using BusinessEntities;
using System.Xml.Serialization;
using System.IO;

namespace MystiflyPartner.BusinessServices
{
    public class MistifyRequest : IMystiflyRequest
    {
        public AirLowFareSearchRS FlightSearchSync(Rootobject message)
        {
            AirLowFareSearchRQ airLowFareSearchRQ = new AirLowFareSearchRQ()
            {
                IsRefundable = Convert.ToBoolean(message.IsRefundable),
                //IsResidentFare = false,
                //NearByAirports = false,
                //PricingSourceType = PricingSourceType.All,
                //RequestOptions = RequestOptions.Fifty,
                SessionId = Getsession("MCN001438", "ONEV2017_xml", "ONEV_XML").SessionId,
                Target = Target.Production,
            };
            string Year = ""; string Month = ""; string Day = "";
            try
            {
                Year = (message.OriginDestinationInformation[0].DepartureDate).Split('-')[2];
                Month = (message.OriginDestinationInformation[0].DepartureDate).Split('-')[1];
                Day = (message.OriginDestinationInformation[0].DepartureDate).Split('-')[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }

            //Origin Destinations start
            OriginDestinationInformation originDestinationInformation = new OriginDestinationInformation()
            {
                DepartureDateTime = Convert.ToDateTime(Year + "-" + Month + "-" + Day),
                DestinationLocationCode = message.OriginDestinationInformation[0].DestinationLocation,
                OriginLocationCode = message.OriginDestinationInformation[0].OriginLocation,
            };
            OriginDestinationInformation[] originDestinationInformations = new OriginDestinationInformation[] { originDestinationInformation };
            airLowFareSearchRQ.OriginDestinationInformations = originDestinationInformations;
            //Origin Destinations End
            

                

            PassengerTypeQuantity passengertypequantity = new PassengerTypeQuantity();

            int TotalAdult = Convert.ToInt16(message.PassengerTypeQuantity.ADT);
            int TotalChild = Convert.ToInt16(message.PassengerTypeQuantity.CHD);
            int TotalInfant = Convert.ToInt16(message.PassengerTypeQuantity.INF);

            int totalPaxTypeCount = 1;
            
            if (TotalChild > 0)
            { totalPaxTypeCount ++; }
            if (TotalInfant > 0)
            { totalPaxTypeCount++; }

            PassengerTypeQuantity[] passengerTypeQuantities = new PassengerTypeQuantity[totalPaxTypeCount];

            passengertypequantity.Code = PassengerType.ADT;
            passengertypequantity.Quantity = TotalAdult;
            passengerTypeQuantities[0] = passengertypequantity;
            if (TotalChild > 0)
            {
                passengertypequantity.Code = PassengerType.CHD;
                passengertypequantity.Quantity = TotalChild;
                passengerTypeQuantities[1] = passengertypequantity;
            }
            if (TotalInfant > 0)
            {
                passengertypequantity.Code = PassengerType.INF;
                passengertypequantity.Quantity = TotalInfant;
                passengerTypeQuantities[2] = passengertypequantity;
            }

            airLowFareSearchRQ.PassengerTypeQuantities = passengerTypeQuantities;
            //PassengerTypeQuantity end
            TravelPreferences travelPreferences = new TravelPreferences();
            string TripType = (message.Triptype).ToUpper();
            if (TripType == "O")
            {
                travelPreferences.AirTripType = AirTripType.OneWay;
            }
            else if (TripType == "R")
            {
                travelPreferences.AirTripType = AirTripType.Return;
            }
            else if (TripType == "M")
            {
                travelPreferences.AirTripType = AirTripType.OpenJaw;
            }
            string Maxstopquantity = message.Maxstopquantity.ToLower();
            if (Maxstopquantity == "direct")
            {
                travelPreferences.MaxStopsQuantity = MaxStopsQuantity.Direct;
            }
            else if (Maxstopquantity == "onestop")
            {
                travelPreferences.MaxStopsQuantity = MaxStopsQuantity.OneStop;
            }
            else if (Maxstopquantity == "all")
            {
                travelPreferences.MaxStopsQuantity = MaxStopsQuantity.All;
            }
            //Travel Preferences start
            //cabin preferences start
            CabinClassPreference cabinClassPreference = new CabinClassPreference();
            string cabintype = (message.cabin).ToUpper();
            if (cabintype == "Y")
            {
                cabinClassPreference.CabinType = CabinType.Y;
            }
            else if (cabintype == "C")
            {
                cabinClassPreference.CabinType = CabinType.C;
            }
            else if (cabintype == "F")
            {
                cabinClassPreference.CabinType = CabinType.F;
            }
            string cabinpreference = (message.PreferenceLevel).ToLower();
            if (cabinpreference == "preferred")
            {
                cabinClassPreference.PreferenceLevel = CabinPreferLevel.Preferred;
            }
            else if (cabinpreference == "restricted")
            {
                cabinClassPreference.PreferenceLevel = CabinPreferLevel.Restricted;
            }
            //cabin preferences end
            Preferences preferences = new Preferences();
            preferences.CabinClassPreference = cabinClassPreference;
            travelPreferences.Preferences = preferences;
            airLowFareSearchRQ.TravelPreferences = travelPreferences;

            XmlSerializer serializer = new XmlSerializer(airLowFareSearchRQ.GetType());
            using (StringWriter writer = new StringWriter())
            {
                serializer.Serialize(writer, airLowFareSearchRQ);

                string test = writer.ToString();
            }
            AirLowFareSearchRS response = new AirLowFareSearchRS();
            try
            {
                OnePointClient client = new OnePointClient("BasicHttpBinding_IOnePoint");
                response = client.AirLowFareSearch(airLowFareSearchRQ);
            }
            catch (Exception ex)
            {

                throw ex;
            }

            return response;
        }
        public async Task<AirLowFareSearchRS> FlightSearch(Rootobject message)
        {
            AirLowFareSearchRQ airLowFareSearchRQ = new AirLowFareSearchRQ()
            {
                IsRefundable = false,
                IsResidentFare = false,
                NearByAirports = false,
                PricingSourceType = PricingSourceType.All,
                RequestOptions = RequestOptions.Fifty,
                SessionId = Getsession("MCN001438", "ONEV2017_xml", "ONEV_XML").SessionId,
                Target = Target.Test,
            };
            //Origin Destinations start
            OriginDestinationInformation originDestinationInformation = new OriginDestinationInformation()
            {
                DepartureDateTime = Convert.ToDateTime("2017-11-20T00:00:00"),
                DestinationLocationCode = "COK",
                OriginLocationCode = "LCY",
            };
            OriginDestinationInformation[] originDestinationInformations = new OriginDestinationInformation[] { originDestinationInformation };
            //originDestinationInformations[0] = originDestinationInformation;
            airLowFareSearchRQ.OriginDestinationInformations = originDestinationInformations;
            //Origin Destinations End

            //PassengerTypeQuantity start
            PassengerTypeQuantity passengertypequantity = new PassengerTypeQuantity()
            {
                Code = PassengerType.ADT,
                Quantity = 1
            };
            PassengerTypeQuantity[] passengerTypeQuantities = new PassengerTypeQuantity[] { passengertypequantity };
            //passengerTypeQuantities[0] = passengertypequantity;
            airLowFareSearchRQ.PassengerTypeQuantities = passengerTypeQuantities;
            //PassengerTypeQuantity end
            //Travel Preferences start
            TravelPreferences travelPreferences = new TravelPreferences()
            {
                AirTripType = AirTripType.OneWay,
                MaxStopsQuantity = MaxStopsQuantity.All,
            };

            // TravelPreferences[] travelPreferenceses = new TravelPreferences[] { };
            //TravelPreferences travelPreferenceses = new TravelPreferences() {
            //    AirTripType =""

            //};
            //travelPreferenceses[0] = travelPreferences;
            //airLowFareSearchRQ.TravelPreferences = travelPreferenceses;
            //Travel Preferences end
            CabinClassPreference[] cabinClassPreferences = new CabinClassPreference[] { };
            //cabin preferences start
            CabinClassPreference cabinClassPreference = new CabinClassPreference()
            {
                CabinType = CabinType.Y,
                PreferenceLevel = CabinPreferLevel.Preferred
            };
            Preferences preferences = new Preferences();
            preferences.CabinClassPreference = cabinClassPreference;

            travelPreferences.Preferences = preferences;


            //cabin preferences end

            //cabinClassPreferences[0] = cabinClassPreference;
            //airLowFareSearchRQ.TravelPreferences.Preferences = cabinClassPreferences;








            //Preferences[] preferences = new Preferences[] { };
            //preferences[0].CabinClassPreference = cabinClassPreference;
            //Travel Preferences End

            OnePointClient client = new OnePointClient("BasicHttpBinding_IOnePoint");
            AirLowFareSearchRS response = await client.AirLowFareSearchAsync(airLowFareSearchRQ);
            return response;
        }

        public SessionCreateRS Getsession(string accountNo, string password, string username)
        {
            SessionCreateRQ sessionCreateRQ = new SessionCreateRQ()
            {
                AccountNumber = accountNo,// "MCN001438",
                Password = password,//"ONEV2017_xml",
                UserName = username//"ONEV_XML"
            };

            OnePointClient Client = new OnePointClient("BasicHttpBinding_IOnePoint");
            return Client.CreateSession(sessionCreateRQ);
        }

        //SessionCreateClientRS Getsession()
        //{
        //    throw new NotImplementedException();
        //}
    }
}
