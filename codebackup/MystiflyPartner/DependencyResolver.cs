﻿namespace MystiflyPartner
{
    using System.ComponentModel.Composition;
    using BusinessServices;
    using BusinessServices.Interface;
    using Resolver;


    [Export(typeof(IComponent))]
    public class DependencyResolver : IComponent
    {
        public void SetUp(IRegisterComponent registerComponent)
        {
            registerComponent.RegisterType<IMystiflyServices, MystiflyServices>();
            registerComponent.RegisterType<IMystiflyRequest, MistifyRequest>();

        }
    }
}
