﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using MystiflyPartner.BusinessServices.Interface;
using PytonPartnerAPI.ErrorHelper;
using BusinessEntities.Entity;

namespace PytonPartnerAPI.Controllers.MystiflyPartner
{
    [RoutePrefix("mystifly-partner")]
    public class MystiflyPartnerController : ApiController
    {
        private IMystiflyServices services;
        public MystiflyPartnerController(IMystiflyServices _services)
        {
            this.services = _services;
        }

        [HttpGet]
        [Route("session")]
        public async Task<HttpResponseMessage> GetSession()
        {
            var data = services.CreateSession("test");
            if (data != null)
                return Request.CreateResponse(HttpStatusCode.OK, data);

            throw new ApiDataException(1000, "Partner not found", HttpStatusCode.NotFound);
        }

        [HttpPost]
        [Route("details")]
        public async Task<HttpResponseMessage> GetData(UserEntity data)
        {
            var response = await services.GetDetails(data);
            if (response != null)
                return Request.CreateResponse(HttpStatusCode.OK, response);

            throw new ApiDataException(1000, "Partner not found", HttpStatusCode.NotFound);
        }

    }
}
