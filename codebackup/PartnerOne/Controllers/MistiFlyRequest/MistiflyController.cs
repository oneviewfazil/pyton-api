﻿using BusinessEntities;
using MystiflyPartner.BusinessServices;
using MystiflyPartner.BusinessServices.Interface;
using PytonPartnerAPI.ErrorHelper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace PytonPartnerAPI.Controllers.MistiFlyRequest
{
    [RoutePrefix("mystifly-partner")]
    public class MistiflyController : ApiController
    {
        private IMystiflyRequest mystiflyRequest;
        public MistiflyController(IMystiflyRequest _mystiflyRequest)
        {
            this.mystiflyRequest = _mystiflyRequest;
        }
        
        [HttpPost]
        [Route("search-flight")]
        public HttpResponseMessage GetFlight(Rootobject data)
        {
            // var response = mystiflyRequest.FlightSearch(data);
            var response = mystiflyRequest.FlightSearchSync(data);
            if (data != null)
                return Request.CreateResponse(HttpStatusCode.OK, response);
            throw new ApiDataException(1000, "Partner not found", HttpStatusCode.NotFound);
        }


        [HttpGet]
        [Route("GetSession/{accountNo}/{password}/{username}")]
        public HttpResponseMessage GetSession(string accountNo, string password, string username)
        {
            var data = mystiflyRequest.Getsession(accountNo, password, username);
            if (data != null)
                return Request.CreateResponse(HttpStatusCode.OK, data);

            throw new ApiDataException(1000, "Partner not found", HttpStatusCode.NotFound);
        }

    }



}
