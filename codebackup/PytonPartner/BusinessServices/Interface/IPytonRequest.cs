﻿using BusinessEntities;
using PytonPartner.PytonPartnerServiceReference;
using System.Threading.Tasks;

namespace PytonPartner.BusinessServices
{
    public interface IPytonRequest
    {
        // string pytonreq();

        FLcommonRS FlightSearch(Rootobject message);

    }
}