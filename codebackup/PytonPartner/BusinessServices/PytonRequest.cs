﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PytonPartner.PytonPartnerServiceReference;
using PytonPartner.BusinessServices.Interface;
using BusinessEntities;
using System.Xml.Serialization;
using System.IO;

namespace PytonPartner.BusinessServices
{
    public class PytonRequest : IPytonRequest
    {
        public FLcommonRS FlightSearch(Rootobject message)
        {

            PytonPartnerServiceReference.WebServiceSoapClient client = new WebServiceSoapClient();

            Origindestinationinformation orgin = new Origindestinationinformation();
            string Year = ""; string Month = ""; string Day = "";
            try
            {
                Year = (message.OriginDestinationInformation[0].DepartureDate).Split('-')[2];
                Month = (message.OriginDestinationInformation[0].DepartureDate).Split('-')[1];
                Day = (message.OriginDestinationInformation[0].DepartureDate).Split('-')[0];

            }
            catch (Exception ex)
            {
                throw ex;
            }





            Passengertypequantity ps = new Passengertypequantity();


            SearchFlightsRequest req = new SearchFlightsRequest();

            req.Departure = message.OriginDestinationInformation[0].OriginLocation;
            req.Destination = message.OriginDestinationInformation[0].DestinationLocation;
            req.DepartureDate = Year + "-" + Month + "-" + Day;

          //  req.ReturnDate = "2017-12-21";
            req.NumADT = message.PassengerTypeQuantity.ADT;
            req.NumINF = message.PassengerTypeQuantity.INF;
            req.NumCHD = message.PassengerTypeQuantity.CHD;

            req.CurrencyCode = CurrencyCode.EUR;
            
          
            req.WaitForResult = true;
            req.NearbyDepartures = false;
            req.NearbyDestinations = false;
            req.RROnly = false;
            req.MetaSearch = false;
            Provider[] prov = new Provider[] {
                 Provider.ElsyArres,
             };
            req.Providers = prov;
            SearchFlights sfl = new SearchFlights();
            sfl.Username = "Albadie";
            sfl.Password = "01E666EA35";
            sfl.LanguageCode = LanguageCode.EN;
            sfl.AppVersion = "";

            sfl.Request = req;
           
           client.SearchFlights(ref sfl);

            var response = sfl.Response;







            {

            }



         

           // SearchFlightsResponse response = new SearchFlightsResponse();


            //return response;
            FLcommonRS fLcommonRS = new FLcommonRS();
            fLcommonRS = PytonTOCommonRs(response);
            //}
            //catch (Exception ex)
            //{

            //    throw ex;
            //}

            return fLcommonRS;
            //throw new NotImplementedException();

        }

        public FLcommonRS PytonTOCommonRs(SearchFlightsResponse response)
        {
            XmlSerializer serializer = new XmlSerializer(response.GetType());

            using (StringWriter writer = new StringWriter())
            {
                serializer.Serialize(writer, response);

                string test = writer.ToString();
            }
            Faremasterpricertravelboardsearchreply faremasterpricertravelboardsearchreply = new Faremasterpricertravelboardsearchreply();
            Replystatus replystatus = new Replystatus();
            FLcommonRS fLcommonRS = new FLcommonRS();


            replystatus.status = null;
            faremasterpricertravelboardsearchreply.replyStatus = replystatus;
            Conversionrate conversionrate = new Conversionrate();
            Conversionratedetail conversionratedetail = new Conversionratedetail()
            {
                currency = "AED"
            };
            //conversionrate.SessionId = MystiFlySessionID;
            conversionrate.conversionRateDetail = conversionratedetail;
            faremasterpricertravelboardsearchreply.conversionRate = conversionrate;
            int totalflights = response.Flights.Length;

            Flightindex[] Flightindexes = new Flightindex[totalflights];
            //Need For each
            Flightindex flightindex = new Flightindex();
            Segmentref segmentref = new Segmentref();
            Typeref typeref = new Typeref();
            Reccount reccount = new Reccount();
            BusinessEntities.Fare fare = new BusinessEntities.Fare();
            Faredetails faredetails = new Faredetails();
            Bagdetails bagdetails = new Bagdetails();
            Description description = new Description();
            Groupofflight groupofflight = new Groupofflight();
            Flightproposal flightproposal = new Flightproposal();
            Flightdetail flightdetail = new Flightdetail();
            Finfo finfo = new Finfo();
            Datetime datetime = new Datetime();
            Location location = new Location();
            Companyid companyid = new Companyid();
            Flightcharacteristics flightcharacteristics = new Flightcharacteristics();
            int resultscound = 1;
            for (int i = 0; i < totalflights; i++)
            {
                Groupofflight[] Groupofflights = new Groupofflight[1];
                flightindex = new Flightindex();
                string flightfromkey = response.Flights[i].Outbound.DepCode;
                string flighttokey = response.Flights[i].Outbound.DestCode;

                string flightdepdatekey = response.Flights[i].Outbound.DepDateTime.ToString("dd-MM-yy-hh-mm-ss");
                string yearkey = flightdepdatekey.Split('-')[2];
                string monthkey = flightdepdatekey.Split('-')[1];
                string daykey = flightdepdatekey.Split('-')[0];
                string hourkey = flightdepdatekey.Split('-')[3];
                string minkey = flightdepdatekey.Split('-')[4];
                string codekey = response.Flights[i].Outbound.CarCode;
                string flightnumberkey = response.Flights[i].Outbound.FlightNo;
                string flightkey = flightfromkey + "-" + flighttokey + "-" + daykey + monthkey + yearkey + "-" + hourkey + minkey + "-" + codekey + "-" + flightnumberkey;
                string FareSourceCodeMysti = "";
                segmentref = new Segmentref();
                segmentref.segRef = FareSourceCodeMysti.ToString();
                segmentref.supplier = "PYWEB";
                segmentref.key = flightkey;
                flightindex.SegmentRef = segmentref;
                typeref = new Typeref()
                {
                    type = "0"
                };
                flightindex.TypeRef = typeref;
                reccount = new Reccount()
                {
                    rec_id = resultscound.ToString(),
                    combi_id = "2",
                };

                string basefare = response.Flights[i].Outbound.FareADT.ToString();
                string currencyCode = response.CurrencyCode.ToString();
                string taxfare = response.Flights[i].Outbound.Taxes.ToString();
                string totalfare = response.Flights[i].TotalFare.ToString();
                fare = new BusinessEntities.Fare()
                {
                    amount = totalfare,
                    taxfare = taxfare,
                    basefare = basefare.ToString(),
                    MarkupFare = "0",
                    currency = currencyCode
                };
                flightindex.fare = fare;
                bagdetails = new Bagdetails()
                {
                    freeAllowance = "",
                    Qcode = "",
                    unit = ""
                };
                flightindex.Bagdetails = bagdetails;
                description = new Description()
                {
                    Pricingmsg = ""
                };
                flightindex.Description = description;

                for (int groups = 0; groups < 1; groups++)
                {
                    groupofflight = new Groupofflight();

                    flightproposal = new Flightproposal()
                    {
                        elapse = "",
                        unitQualifier = "",
                        VAirline = "",
                        Ocarrier = "",
                    };
                    groupofflight.FlightProposal = flightproposal;
                    //flight details Loop strat
                    //int Totalflightsegment = Convert.ToInt16(response.PricedItineraries[flights].OriginDestinationOptions[groups].FlightSegments.Length);
                    Flightdetail[] flightdetails = new Flightdetail[1];



                    string AirlegDepdatetime = response.Flights[i].Outbound.DepDateTime.ToString("dd-MM-yy-hh-mm-ss");
                    string AirlegArrdatetime = response.Flights[i].Outbound.ArrDateTime.ToString("dd-MM-yy-hh-mm-ss");

                    string AirlegDepYear = AirlegDepdatetime.Split('-')[2];
                    string AirlegDepMonth = AirlegDepdatetime.Split('-')[1];
                    string AirlegDepDay = AirlegDepdatetime.Split('-')[0];
                    string AirlegDepHour = AirlegDepdatetime.Split('-')[3];
                    string AirlegDepMinute = AirlegDepdatetime.Split('-')[4];

                    string AirlegArrYear = AirlegArrdatetime.Split('-')[2];
                    string AirlegArrMonth = AirlegArrdatetime.Split('-')[1];
                    string AirlegArrDay = AirlegArrdatetime.Split('-')[0];
                    string AirlegArrHour = AirlegArrdatetime.Split('-')[3];
                    string AirlegArrMinute = AirlegArrdatetime.Split('-')[4];

                    string AirLegDuration = response.Flights[i].Outbound.Duration;
                    // TimeSpan span = TimeSpan.FromMinutes(Convert.ToDouble(AirLegDuration));
                    string AirLegElapse = AirLegDuration;


                    flightdetail = new Flightdetail();
                    finfo = new Finfo();
                    datetime = new Datetime()
                    {
                        Depdate = AirlegDepDay + AirlegDepMonth + AirlegDepYear,
                        Deptime = AirlegDepHour + AirlegDepMinute,
                        Arrdate = AirlegArrDay + AirlegArrMonth + AirlegArrYear,
                        Arrtime = AirlegArrHour + AirlegArrMinute,
                        Variation = AirLegElapse
                    };
                    finfo.DateTime = datetime;
                    string AirLegDepFrom = response.Flights[i].Outbound.DepCode;
                    string AirLegArrFrom = response.Flights[i].Outbound.DestCode;
                    location = new Location()
                    {
                        locationFrom = AirLegDepFrom,
                        LocationTo = AirLegArrFrom,
                        Fromterminal = "",
                        Toterminal = ""
                    };
                    finfo.location = location;
                    string Mcarriercode = "NA";
                    string Ocarriercode = "NA";
                    companyid = new Companyid()
                    {
                        mCarrier = Mcarriercode,
                        oCarrier = Ocarriercode
                    };
                    finfo.companyId = companyid;
                    string Flnumber = response.Flights[i].Outbound.FlightNo;
                    string Fleqpment = "NA";
                    bool Fleticketing = false;


                    finfo.flightNo = Flnumber;
                    finfo.eqpType = Fleqpment;
                    finfo.eTicketing = Fleticketing.ToString();
                    finfo.attributeType = "";
                    finfo.Elapsedtime = AirLegElapse;
                    flightdetail.Finfo = finfo;
                    flightcharacteristics = new Flightcharacteristics()
                    {
                        inFlightSrv = ""
                    };

                    flightdetail.flightCharacteristics = flightcharacteristics;
                    flightdetails[0] = flightdetail;

                    groupofflight.flightDetails = flightdetails;
                    Groupofflights[groups] = groupofflight;
                }
                flightindex.groupOfFlights = Groupofflights;

                //group here

                Flightindexes[i] = flightindex;
                resultscound++;
            }
            faremasterpricertravelboardsearchreply.flightIndex = Flightindexes;
            fLcommonRS.fareMasterPricerTravelBoardSearchReply = faremasterpricertravelboardsearchreply;
            //  string CommonSearchRS = JsonConvert.SerializeObject(fLcommonRS, Newtonsoft.Json.Formatting.Indented, new JsonSerializerSettings() { DefaultValueHandling = DefaultValueHandling.Include });          
            return fLcommonRS;

            // throw new NotImplementedException();
        }
    }
}
    

