﻿
using System;
using System.Collections.Generic;
using System.Linq;
using PytonPartnerAPI;
using PytonPartner.BusinessServices;
using PytonPartner.BusinessServices.Interface;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using PytonPartner.PytonPartnerServiceReference;
using BusinessEntities;
using PytonPartnerAPI.ErrorHelper;

namespace PytonPartnerAPI.Controllers
{
    [RoutePrefix("python-partner")]
    public class pytonrequestController : ApiController
    {
        private IPytonRequest  pytonRequest;

        public pytonrequestController()
        {
            pytonRequest = new PytonPartner.BusinessServices.PytonRequest();
        }

        [HttpPost]
        [Route("search-flight")]
        public HttpResponseMessage getflight(Rootobject data)
        {
            // var response = mystiflyRequest.FlightSe arch(data);

            var response = pytonRequest.FlightSearch(data);
           
                return Request.CreateResponse(HttpStatusCode.OK, response);
            throw new ApiDataException1(1000, "Partner not found", HttpStatusCode.NotFound);
        }
        public string Getsession(string Name)
        {
            return "";
        }
       
    }
}
