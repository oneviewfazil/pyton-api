﻿using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using BAL.BusinessServices.Interface;
using BusinessEntities.Entity;
using PytonPartnerAPI.ErrorHelper;

namespace PytonPartnerAPI.Controllers
{
    [RoutePrefix("partnertwo")]
    public class PartnerController : ApiController
    {
        private IPartnerServices services;
        public PartnerController(IPartnerServices _services)
        {
            this.services = _services;
        }

        [HttpGet]
        [Route("all/async")]
        [ResponseType(typeof(PartnerEntity))]
        public async Task<HttpResponseMessage> GetPartnerAsync()
        {
            var data = await services.GetPartners();
            if (data != null)
                return Request.CreateResponse(HttpStatusCode.OK, data);

            throw new ApiDataException(1000, "Partner not found", HttpStatusCode.NotFound);
        }

        [HttpGet]
        [Route("all/sync")]
        [ResponseType(typeof(PartnerEntity))]
        public HttpResponseMessage GetPartnerSync()
        {
            var data = services.GetPartnersSync();
            if (data != null)
                return Request.CreateResponse(HttpStatusCode.OK, data);

            throw new ApiDataException(1000, "Partner not found", HttpStatusCode.NotFound);
        }
    }
}
